package Common.core.Configuration;

/**
 * Created by praveen on 2/12/2015.
 */
public class POMConfiguration extends AbstractConfiguration {

    private String browser;
    private String browserVer;
    private String platform;
    private String platformVer;
    private String env;
    private boolean saucemode;
    private String sauce_name;
    private String sauce_key;
    private String credentialsFilePath;
    private String baseUrl;

    public POMConfiguration() {
        baseUrl=System.getProperty("base-address");
        saucemode = Boolean.parseBoolean(System.getProperty("saucemode"));

            browser=System.getenv("SELENIUM_BROWSER");
            if(browser == null || browser.isEmpty())
            {
                browser = System.getProperty("browser");
                if (browser == null || browser.isEmpty())
                {
                    browser = "Chrome"; //Set default value to Chrome;
                }
            }

        browserVer=System.getenv("SELENIUM_VERSION");
        if(browserVer == null || browser.isEmpty())
        {
            browserVer = System.getProperty("browserVer");
            if (browserVer == null || browser.isEmpty())
            {
                browserVer = "49"; //Set default value to Chrome;
            }
        }

        platform=System.getenv("SELENIUM_PLATFORM");
        if(platform == null || platform.isEmpty())
        {
            platform = System.getProperty("platform");
            if (platform == null || browser.isEmpty())
            {
                platform = "ANY"; //Set default value to Chrome;
            }
        }


        env = System.getProperty("env");
        if (env == null || env.isEmpty()) {
            env = "prod"; //Set default value to production
        }



        platformVer=System.getProperty("platform-version");
        if(platformVer==null || platformVer.isEmpty() )
        {
            platformVer="ANY";
        }

        credentialsFilePath = System.getProperty("config");
        sauce_name = System.getProperty("saucename");
        sauce_key = System.getProperty("saucekey");
    }

    @Override
    public String getBrowser() {

        return this.browser;
    }

    @Override
    public String getbrowserVer() {
        return this.browserVer;   }

    @Override
    public String getEnv() {
        return this.env;
    }

    @Override
    public String getPlatformVersion() {  return this.platformVer ;  }

    @Override
    public String getPlatform() {
        return this.platform;
    }

    @Override
    public boolean getSaucemode() {

        return this.saucemode;
    }

    @Override
    public String getCredentialsFilePath() { return this.credentialsFilePath; }

    @Override
    public String getSaucename() {return this.sauce_name;}

    @Override
    public String getSaucekey() {return this.sauce_key; }

    @Override
    public String getBaseUrl() {return this.baseUrl; }





}


