package Common.Templates;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class NewTestTemplateBeforeClass extends  NewTestTemplateCore {

  public NewTestTemplateBeforeClass() {
    super();
  }

  @BeforeMethod(alwaysRun = true)
  public void start() {
    prepareURLs();
   startBrowser();

    //logOut();
  }

  @AfterMethod  (alwaysRun = true)
  public void stop() {
    stopBrowser();
  }
}
