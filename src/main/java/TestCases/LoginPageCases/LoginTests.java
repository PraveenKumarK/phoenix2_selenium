package TestCases.LoginPageCases;

import Common.Templates.NewTestTemplateBeforeClass;
import Common.core.Assertion;
import Common.dataProvider.ReadExcelDataProvider;
import PageObjects.BasePageObject;
import PageObjects.HomePage.HomePageObjects;
import PageObjects.LoginPage.LoginPageObjects;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by praveenk on 09-Feb-17.
 */
public class LoginTests  extends NewTestTemplateBeforeClass {

    public LoginTests()
    {

    }

    @Test(groups = "w")

    public void test_to_check_mandatory_field_alert_in_login()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.userName.sendKeys("");
        loginPageObjects.tab_press();

        Assertion.assertTrue(driver.getPageSource().contains("Please enter your username."));
    //    Assertion.assertEquals("Please enter your username.",loginPageObjects.capture_alert_username(configY.get("mandatory_alert")));
    }

    @Test()
    public void test_to_check_mandatory_field_alert_in_password()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.passWord.sendKeys("");
        loginPageObjects.passWord.sendKeys(Keys.TAB);
        Assert.assertEquals("Please enter your password.",loginPageObjects.capture_alert_password(configY.get("mandatory_alert_password")));
    }


    @Test(enabled = false)
    public void test_to_check_Error_alert_displayed_for_login_fail() throws InterruptedException {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix(String.valueOf(configY.get("loginid")),String.valueOf(configY.get("admin_password")));
        Assert.assertEquals("Please check your username/admin_password !!!",loginPageObjects.capture_error_alert(configY.get("login_fail")));
    }
    @Test()
    public void test_to_check_alert_for_email_validation() throws InterruptedException {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.userName.sendKeys("invalid");
        loginPageObjects.userName.sendKeys(Keys.TAB);
        Assert.assertEquals(configY.get("emailalert"),loginPageObjects.capture_alert_username(configY.get("emailalert")));
    }

    @Test

    public void Validation_mandatory_fields()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_business_entity_page();
        homePageObjects.open_BE_add_page();
        homePageObjects.Validation_mandatory();
        }


    @Test(dataProvider="getData")
    public void add_business_entity(String bname,String Regnum,String strtAdddress1,String area,String landmark
    ,String state,String city,String pincode1,String mot,String apprxSpSz) throws InterruptedException
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_business_entity_page();
        homePageObjects.open_BE_add_page();
        homePageObjects.fill_business_name(bname);
        homePageObjects.fill_RegistrationNumber(Regnum);
        homePageObjects.fill_StreetAddress1(strtAdddress1);
        homePageObjects.fill_Area1(area);
        homePageObjects.fill_Landmark1(landmark);
        homePageObjects.fill_State(state);
        homePageObjects.fill_City1(city);
        homePageObjects.fill_Pincode1(pincode1);
        homePageObjects.fill_mot(mot);
        homePageObjects.fill_shopSize(apprxSpSz);
      // homePageObjects.fill_YearOFES(yob);
    }
    /*@DataProvider(name = "getData")
    public Object[][] loginData() {
        Object[][] arrayObject = ReadExcelDataProvider.getExcelData("11.xls", "Sheet1");
        return arrayObject;
    }*/

}
