package TestCases.ClientSetupCases;

import Common.Templates.NewTestTemplateBeforeClass;
import PageObjects.BasePageObject;
import PageObjects.ClientSetUp.ClientListPageObjects;
import PageObjects.HomePage.HomePageObjects;
import PageObjects.LoginPage.LoginPageObjects;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by praveenk on 13-Mar-17.
 */
public class ClientListCases extends NewTestTemplateBeforeClass {

    @Test()
    public void verify_elements_in_client_list_page()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_Client_Setup_page();
        ClientListPageObjects clientListPageObjects=new ClientListPageObjects(driver);
        clientListPageObjects.verify_ClientList_page_URL();
        clientListPageObjects.verify_ClientList_PageTitle();
        clientListPageObjects.verify_all_breadcrumb_links() ;
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        clientListPageObjects.verify_state_drop_down_shows_all_state() ;
        clientListPageObjects.verify_buttons_list_page();
        clientListPageObjects.ClientTable_list_data();

    }

    @Test
    public  void verify_search_in_Client_list_returns_result()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_Client_Setup_page();
        ClientListPageObjects clientListPageObjects=new ClientListPageObjects(driver);
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        String totalRecords = clientListPageObjects.get_total_Records();
        // clientListPageObjects.ClientTable_list_data();
        clientListPageObjects.search_Client("mat lam tam");
        clientListPageObjects.verify_search_result("mat lam tam",totalRecords);
    }

    @Test
    public  void verify_reset_button_in_client_list_page()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_Client_Setup_page();
        ClientListPageObjects clientListPageObjects=new ClientListPageObjects(driver);
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        String totalRecords = clientListPageObjects.get_total_Records();
        // clientListPageObjects.ClientTable_list_data();
        clientListPageObjects.search_Client("mat lam tam");
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        clientListPageObjects.verify_search_result("mat lam tam",totalRecords);
        String ResettotalRecords = clientListPageObjects.get_total_Records();
        clientListPageObjects.clickReset();
        clientListPageObjects.verifyReset(ResettotalRecords);
    }



}
