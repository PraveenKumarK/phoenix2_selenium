package TestCases.ClientSetupCases;

import Common.Templates.NewTestTemplateBeforeClass;
import Common.core.Assertion;
import Common.dataProvider.ReadExcelDataProvider;
import PageObjects.BasePageObject;
import PageObjects.ClientSetUp.AddClientPageObjects;
import PageObjects.ClientSetUp.ClientListPageObjects;
import PageObjects.HomePage.HomePageObjects;
import PageObjects.LoginPage.LoginPageObjects;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;

/**
 * Created by praveenk on 01-Mar-17.
 */
public class AddClientCases extends NewTestTemplateBeforeClass {

    public AddClientCases(){}


    @DataProvider(name = "getData")
    public Object[][] loginData() {
        Object[][] arrayObject = ReadExcelDataProvider.getExcelData("Clientdata.xls", "data");
        System.out.println(arrayObject.length);
        return arrayObject;
    }
    @Test(dataProvider = "getData")

    public void Add_Client_and_verify_the_added_data(String Client_Code,String Client_Name,String Tax_Account_Number,String Sales_Tax_Registration_Number,
                           String Client_Images,String Banner_Images,String Brand_Color,
                           String Program_Setup_Url,String Program_Operations_Url,
                           String Street_Address_OfficeAddress,String Area_OfficeAddress,String Landmark_OfficeAddress,String State_OfficeAddress,
                           String City_OfficeAddress,String Pincode_OfficeAddress,String Street_Address_CommunicationAddress,String Area_CommunicationAddress,
                           String Landmark_CommunicationAddress,String State_CommunicationAddress,String City_CommunicationAddress,String Pincode_CommunicationAddress,
                           String Person_Name_Contact,String Mobile_Number_Contact,String Office_Number_Contact,String Email_Contact,String Bname,
                           String Bheadname,String BranchContactNumber,String BranchEmail2,String Street_Address_Branch,String Area_Branch,
                           String Landmark_Branch,String State_Branch,String City_Branch,String Pincode_Branch,String Fname,String lname,
                           String email_admin,String Admin_mobile,String password
    ) throws InterruptedException, AWTException {

        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_Client_Setup_page();
        AddClientPageObjects addClientPageObjects=new AddClientPageObjects(driver);
        ClientListPageObjects clientListPageObjects=new ClientListPageObjects(driver);
        clientListPageObjects.clickAdd();
        addClientPageObjects.fill(Client_Code,Client_Name,Tax_Account_Number,Sales_Tax_Registration_Number,
                Client_Images,Banner_Images,Brand_Color,Program_Setup_Url,
                Program_Operations_Url,Street_Address_OfficeAddress,Area_OfficeAddress,Landmark_OfficeAddress,
                State_OfficeAddress,City_OfficeAddress,Pincode_OfficeAddress,Street_Address_CommunicationAddress,Area_CommunicationAddress,
                Landmark_CommunicationAddress,State_CommunicationAddress,City_CommunicationAddress,Pincode_CommunicationAddress,Person_Name_Contact,
                Mobile_Number_Contact,Office_Number_Contact,Email_Contact,Bname,Bheadname,
                BranchContactNumber,BranchEmail2,Street_Address_Branch,Area_Branch,Landmark_Branch,State_Branch,City_Branch,Pincode_Branch
                ,Fname,lname,email_admin,Admin_mobile,password
        );
        addClientPageObjects.fillAndSubmit();
        Assert.assertEquals(addClientPageObjects.verifyClientAddedAlert(),"Client record successfully created");
        homePageObjects.open_home_page();
        homePageObjects.open_Client_Setup_page();
        clientListPageObjects.search_Client(Client_Name);
        clientListPageObjects.verifyPageLoaded();
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        clientListPageObjects.clickRadio();
        clientListPageObjects.clickEdit();
        AddClientPageObjects addClientPageObjects1=new AddClientPageObjects(driver);
        addClientPageObjects1.verify_the_added_client_data(Client_Code,Client_Name,Tax_Account_Number,Sales_Tax_Registration_Number,
                Client_Images,Banner_Images,Brand_Color,Program_Setup_Url,
                Program_Operations_Url,Street_Address_OfficeAddress,Area_OfficeAddress,Landmark_OfficeAddress,
                State_OfficeAddress,City_OfficeAddress,Pincode_OfficeAddress,Street_Address_CommunicationAddress,Area_CommunicationAddress,
                Landmark_CommunicationAddress,State_CommunicationAddress,City_CommunicationAddress,Pincode_CommunicationAddress,Person_Name_Contact,
                Mobile_Number_Contact,Office_Number_Contact,Email_Contact,Bname,Bheadname,
                BranchContactNumber,BranchEmail2,Street_Address_Branch,Area_Branch,Landmark_Branch,State_Branch,City_Branch,Pincode_Branch
                ,Fname,lname,email_admin,Admin_mobile,password);



    }






    @Test
    public void verify_all_fields_are_validated_in_add_client_details_page()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_Client_Setup_page();
        ClientListPageObjects clientListPageObjects=new ClientListPageObjects(driver);
        AddClientPageObjects addClientPageObjects=new AddClientPageObjects(driver);
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        clientListPageObjects.clickAdd();
        addClientPageObjects.Validate_all_fields_in_AddClientPage();


    }

    @Test
    public void verify_left_menu_shows_all_link_in_edit_clientDetails_page()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_Client_Setup_page();
        ClientListPageObjects clientListPageObjects=new ClientListPageObjects(driver);
        AddClientPageObjects addClientPageObjects=new AddClientPageObjects(driver);
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        clientListPageObjects.clickRadio();
        clientListPageObjects.clickEdit();
        addClientPageObjects.verifyLeftMenu();

    }
}
