package TestCases.ClientSetupCases;

import Common.Templates.NewTestTemplateBeforeClass;
import Common.Templates.NewTestTemplateCore;
import PageObjects.BasePageObject;
import PageObjects.ClientSetUp.AddClientPageObjects;
import PageObjects.ClientSetUp.ClientListPageObjects;
import PageObjects.ClientSetUp.DivisionMasterSetupPageObjects;
import PageObjects.HomePage.HomePageObjects;
import PageObjects.LoginPage.LoginPageObjects;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by praveenk on 16-Mar-17.
 */
public class ClientUserSetupCases extends NewTestTemplateBeforeClass {

    @Test()
    public void verify_elements_in_DivisionMaster_page()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.open_Client_Setup_page();
        ClientListPageObjects clientListPageObjects=new ClientListPageObjects(driver);
        AddClientPageObjects addClientPageObjects=new AddClientPageObjects(driver);
        Assert.assertTrue(clientListPageObjects.verify_headers_present());
        clientListPageObjects.clickRadio();
        clientListPageObjects.clickEdit();
        addClientPageObjects.OpenDivisonMasterPage();
        DivisionMasterSetupPageObjects divisionMasterSetupPageObjects=new DivisionMasterSetupPageObjects(driver);
        divisionMasterSetupPageObjects.verify_DivisionMasterSetupPageURL()  ;
        divisionMasterSetupPageObjects.verify_all_breadcrumb_links() ;
        divisionMasterSetupPageObjects.verify_division_master_page_title() ;



    }
}
