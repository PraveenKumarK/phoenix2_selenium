package TestCases.ProgramSetUpCases;

import Common.Templates.NewTestTemplateBeforeClass;
import Common.Templates.NewTestTemplateCore;
import PageObjects.BasePageObject;
import PageObjects.HomePage.HomePageObjects;
import PageObjects.LoginPage.LoginPageObjects;
import PageObjects.ProgramSetUp.ProgramAddPageObjects;
import PageObjects.ProgramSetUp.ProgramSetUpPageObjects;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by praveenk on 22-Mar-17.
 */
public class ProgramSetupListCases extends NewTestTemplateBeforeClass {

    @Test()
    public void verify_elements_in_program_setup_list_page()
    {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.openProgramSetupPage();

        ProgramSetUpPageObjects programSetUpPageObjects =new ProgramSetUpPageObjects(driver);
        programSetUpPageObjects.verifyPageLoaded();
        programSetUpPageObjects.verifyProgramSetupPageUrl();
        programSetUpPageObjects.verifyBreadCrumbList();
        programSetUpPageObjects.verify_buttons_list_page();
        Assert.assertTrue(programSetUpPageObjects.verify_headers_present());
        programSetUpPageObjects.programListTable();



    }

    @Test()
    public void verify_elements_in_program_setup_add_page() throws InterruptedException {
        BasePageObject base = new BasePageObject(driver);
        LoginPageObjects loginPageObjects=new LoginPageObjects(driver);
        base.open_basepage();
        loginPageObjects.login_phoenix("kuldip@annectos.in", "123456");
        HomePageObjects homePageObjects=new HomePageObjects(driver);
        homePageObjects.openProgramSetupPage();
        ProgramSetUpPageObjects programSetUpPageObjects =new ProgramSetUpPageObjects(driver);
        programSetUpPageObjects.verifyPageLoaded();
        programSetUpPageObjects.clickAddNewButton();
        ProgramAddPageObjects programAddPageObjects=new ProgramAddPageObjects(driver);
        programAddPageObjects.fill();
        Thread.sleep(1000);



    }
}
