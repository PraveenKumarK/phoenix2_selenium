package PageObjects.HomePage;

import PageObjects.BasePageObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by praveenk on 22-Feb-17.
 */
public class HomePageObjects extends BasePageObject {

    public HomePageObjects(WebDriver driver) {
        super(driver);
    }


    @FindBy(id="innerPageHeaderLogo")
    public WebElement Phoenix_logo;


    @FindBy(id = "businessEntityImgDiv")
    public WebElement business_entity_link;

    @FindBy(xpath=" //a[@href='/businessEntity/add'] ")
    public WebElement add_BE_link;

    @FindBy(id="clientSetupImgDiv")
    public WebElement clientSetupPage;

    @FindBy(id="programSetupImgDiv")
    public WebElement programSetupPage;



    @FindBy(xpath = "//div[contains(@class, 'alert alert-danger inputValidationErr ')]")
    public List<WebElement> BE_page_alert;


    //BUSINESS ENTITY labels and Input fields

    @FindBy(id = "businessName")
    public WebElement enterBusinessName;

    @FindBy(xpath = "//label[contains(@for,'businessName')]")
    public WebElement BusinessName_label;

    @FindBy(id = "registrationNumber")
    public WebElement enterRegistrationNumber;

    @FindBy(xpath = "//label[contains(@for,'registrationNumber')]")
    public WebElement RegistrationNumber_label;

    @FindBy(id = "streetAddress")
    public WebElement enterStreetAddress;

    @FindBy(xpath = "//label[contains(@for,'streetAddress')]")
    public WebElement StreetAddress1_label;

    @FindBy(id = "area")
    public WebElement enterArea;

    @FindBy(xpath = "//label[contains(@for,'area')]")
    public WebElement Area1_label;

    @FindBy(id = "landmark")
    public WebElement enterLandmark;

    @FindBy(xpath = "//label[contains(@for,'landmark')]")
    public WebElement Landmark1_label;

    @FindBy(id = "state")
    public WebElement selectState;

    @FindBy(xpath = "//label[contains(@for,'state')]")
    public WebElement State_label;

    @FindBy(id = "city")
    public WebElement SelectCity;

    @FindBy(xpath = "//label[contains(@for,'city')]")
    public WebElement City_label;

    @FindBy(id = "pincode")
    public WebElement enterPinCode1;

    @FindBy(xpath = "//label[contains(@for,'pincode')]")
    public WebElement pincode_label;

    @FindBy(id = "monthlyTurnOver")
    public WebElement enterMonthlyTurnOverInRupees;

    @FindBy(xpath = "//label[contains(@for,'monthlyTurnOver')]")
    public WebElement monthlyTurnOver_label;

    @FindBy(id = "approxShopSize")
    public WebElement enterShopSize;

    @FindBy(xpath = "//label[contains(@for,'approxShopSize')]")
    public WebElement approxShopSize_label;

    @FindBy(id = "yearOfEstablishment")
    public WebElement enterYearsOfEstablishment;

    @FindBy(xpath = "//label[contains(@for,'yearOfEstablishment')]")
    public WebElement yearOfEstablishment_label;

    @FindBy(id = "dealsWithBrands")
    public WebElement enterDealsWithBrands;

    @FindBy(xpath = "//label[contains(@for,'dealsWithBrands')]")
    public WebElement dealsWithBrands_label;

    @FindBy(xpath = "//button[text()=' + ']")
    public WebElement noName;

    //BUSINESS ENTITY USER 1 labels and Input fields

    @FindBy(xpath = "//label[contains(@for,'admin_firstName')]")
    public WebElement FirstName1_label;

    @FindBy(id = "admin_firstName")
    public WebElement enterFirstName1;

    @FindBy(xpath = "//label[contains(@for,'admin_lastName')]")
    public WebElement LastName1_label;

    @FindBy(id = "admin_lastName")
    public WebElement enterLastName1;

    @FindBy(xpath = "//label[contains(@for,'userType')]")
    public WebElement userType1_label;

    @FindBy(xpath = "//input[formcontrolname=\"userType\"]")
    public WebElement userType1;

    @FindBy(xpath = "//label[contains(@for,'Gender')]")
    public WebElement gender1_label;

    @FindBy(xpath = "//*[@id=\"gender\" and @value=\"male\"]")
    public WebElement radiogenderMale1;

    @FindBy(xpath = "//*[@id=\"gender\" and @value=\"female\"]")
    public WebElement radiogenderFeMale1;

    @FindBy(xpath = "//label[contains(@for,'dateOfBirth')]")
    public WebElement dob1_label;

    @FindBy(id = "dateOfBirth")
    public WebElement enterBirthDate1;

    @FindBy(xpath = "//label[contains(@for,'maritalStatus')]")
    public WebElement maritalStatus1_label;

    @FindBy(xpath = "//*[@id=\"maritalStatus\" and @value=\"married\"]")
    public WebElement radioMarried1;

    @FindBy(xpath = "//*[@id=\"maritalStatus\" and @value=\"single\"]")
    public WebElement radioSingle1;


    @FindBy(xpath = "//label[contains(@for,'email')]")
    public WebElement email1_label;

    @FindBy(id = "email")
    public WebElement enterEmail1;

    @FindBy(xpath = "//label[contains(@for,'mobileNumber')]")
    public WebElement mobileNumber1_label;

    @FindBy(id = "mobileNumber")
    public WebElement enterMobileNumber1;

    @FindBy(xpath = "//label[contains(@for,'alternateMobileNumber')]")
    public WebElement alternateMobileNumber1_label;

    @FindBy(id = "alternateMobileNumber")
    public WebElement enterAlternateMobileNumber1;

    @FindBy(xpath = "//label[contains(@for,'streetAddress')]")
    public WebElement streetAddress1_label;

    @FindBy(id = "streetAddress")
    public WebElement enterStreetAddress1;

    @FindBy(xpath = "//label[contains(@for,'area')]")
    public WebElement area1_label;

    @FindBy(id = "area")
    public WebElement enterArea1;

    @FindBy(xpath = "//label[contains(@for,'landmark')]")
    public WebElement landmark1_label;

    @FindBy(id = "landmark")
    public WebElement enterLandmark1;

    @FindBy(id = "state2")
    public WebElement selectState1;

    @FindBy(id = "communicationCity")
    public WebElement selectCity1;

    @FindBy(id = "pincode")
    public WebElement enterPinCode2;

    @FindBy(id = "admin_password")
    public WebElement enterPassword;

    @FindBy(xpath = "//button[text()='Generate Password']")
    public WebElement generatePassword;

    @FindBy(xpath = "//button[text()='Add User']")
    public WebElement addUser;

    @FindBy(id = "kycStatus")
    public WebElement pleaseAddName10;

    @FindBy(xpath = "//button[text()='CONFIRM']")
            public WebElement cONFIRM;

            @FindBy(xpath = "//button[text()='Submit']")
            public WebElement submit;

            @FindBy(xpath = "//button[text()='Reset']")
            public WebElement reset;

    public void open_business_entity_page()
    {
        waitForElementByElement(business_entity_link);
        business_entity_link.click();

    }

    public  void open_BE_add_page()
    {
        waitForElementByElement(add_BE_link);
        add_BE_link.click();
    }

    public HomePageObjects open_home_page()
    {

        waitForElementByElement(Phoenix_logo);
        Phoenix_logo.click();
        return this;

    }

    public  void open_Client_Setup_page()
    {
        waitForElementByElement(clientSetupPage);
        clientSetupPage.click();
    }

    /**
     * Open Program Set up page
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public  void openProgramSetupPage()
    {
        waitForElementByElement(programSetupPage);
        programSetupPage.click();
    }



    public void validate_businessname_length()
    {
        waitForElementByElement(enterBusinessName);
        enterBusinessName.sendKeys("123");
        enterBusinessName.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, "Business name should be of atleast 4 characters");
    }

    public void validate_businessname_field_is_mandatory()
    {
        waitForElementByElement(enterBusinessName);
        enterBusinessName.sendKeys("");
        enterBusinessName.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(BusinessName_label.getText()));

    }

    public void fill_business_name(String b)
    {
        waitForElementByElement(enterBusinessName);
        enterBusinessName.sendKeys(b);

    }

    public void validate_RegistrationNumber_field_is_mandatory()
    {
        waitForElementByElement(enterRegistrationNumber);
        enterRegistrationNumber.sendKeys("");
        enterRegistrationNumber.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(RegistrationNumber_label.getText()));

    }

    public void fill_RegistrationNumber(String b)
    {
        waitForElementByElement(enterRegistrationNumber);
        enterRegistrationNumber.sendKeys(b);

    }
    public void validate_StreetAddress1_field_is_mandatory()
    {
        waitForElementByElement(enterStreetAddress1);
        enterStreetAddress1.sendKeys("");
        enterStreetAddress1.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(StreetAddress1_label.getText()));

    }
    public void fill_StreetAddress1(String b)
    {
        waitForElementByElement(enterStreetAddress1);
        enterStreetAddress1.sendKeys(b);

    }

    public void validate_Area1_field_is_mandatory()
    {
        waitForElementByElement(enterArea1);
        enterArea1.sendKeys("");
        enterArea1.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(Area1_label.getText()));

    }
    public void fill_Area1(String b)
    {
        waitForElementByElement(enterArea1);
        enterArea1.sendKeys(b);

    }

    public void validate_Landmark1_field_is_mandatory()
    {
        waitForElementByElement(enterLandmark1);
        enterLandmark1.sendKeys("");
        enterLandmark1.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(Landmark1_label.getText()));

    }

    public void fill_Landmark1(String b)
    {
        waitForElementByElement(enterLandmark1);
        enterLandmark1.sendKeys(b);

    }

    public void validate_State_field_is_mandatory()
    {
        waitForElementByElement(selectState);
        selectState.sendKeys("");
        selectState.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(State_label.getText()));

    }

    public void fill_State(String b)
    {
        waitForElementByElement(selectState);
        Select_Drop(selectState).selectByIndex(0);


    }

    public void validate_City1_field_is_mandatory()
    {
        waitForElementByElement(SelectCity);
        SelectCity.sendKeys("");
        SelectCity.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(City_label.getText()));

    }
    public void fill_City1(String b)
    {
        waitForElementByElement(SelectCity);
        Select_Drop(SelectCity).selectByIndex(0);

    }

    public void validate_Pincode1_field_is_mandatory()
    {
        waitForElementByElement(enterPinCode1);
        enterPinCode1.sendKeys("");
        enterPinCode1.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(pincode_label.getText()));

    }
    public void fill_Pincode1(String b)
    {
        waitForElementByElement(enterPinCode1);
        enterPinCode1.sendKeys(b);

    }

    public void validate_MonthlyTurnOver_field_is_mandatory()
    {
        waitForElementByElement(enterMonthlyTurnOverInRupees);
        enterMonthlyTurnOverInRupees.sendKeys("");
        enterMonthlyTurnOverInRupees.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(monthlyTurnOver_label.getText()));

    }

    public void fill_mot(String b)
    {
        waitForElementByElement(enterMonthlyTurnOverInRupees);
        enterMonthlyTurnOverInRupees.sendKeys(b);

    }

    public void validate_ShopSize_field_is_mandatory()
    {
        waitForElementByElement(enterShopSize);
        enterShopSize.sendKeys("");
        enterShopSize.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(approxShopSize_label.getText()));

    }

    public void fill_shopSize(String b)
    {
        waitForElementByElement(enterShopSize);
        enterShopSize.sendKeys(b);

    }

    public void validate_YearOFES_field_is_mandatory()
    {
        waitForElementByElement(enterYearsOfEstablishment);
        enterYearsOfEstablishment.sendKeys("");
        enterYearsOfEstablishment.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(yearOfEstablishment_label.getText()));

    }

    public void fill_YearOFES(String b)
    {
        waitForElementByElement(enterYearsOfEstablishment);
        enterYearsOfEstablishment.sendKeys(b);

    }

    public void validate_fname1()
    {
         waitForElementByElement(enterFirstName1);
        enterFirstName1.sendKeys("");
        enterFirstName1.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(enterFirstName1.getText()));

    }

    public void fill_FirstName1(String b)
    {
        waitForElementByElement(enterFirstName1);
        enterFirstName1.sendKeys(b);

    }

    public void validate_LastName1()
    {
        waitForElementByElement(enterLastName1);
        enterLastName1.sendKeys("");
        enterLastName1.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(enterLastName1.getText()));

    }

    public void fill_LastName1(String b)
    {
        waitForElementByElement(enterLastName1);
        enterLastName1.sendKeys(b);

    }

    public void validate_gender1()
    {
        waitForElementByElement(radiogenderMale1);
        enterLastName1.sendKeys("");
        enterLastName1.sendKeys(Keys.TAB);
        getElementByText(BE_page_alert, remove_last(radiogenderMale1.getText()));
    }



    public void Validation_mandatory()
    {
        validate_businessname_field_is_mandatory();
        validate_RegistrationNumber_field_is_mandatory();
        validate_StreetAddress1_field_is_mandatory();
        validate_City1_field_is_mandatory();
        validate_Landmark1_field_is_mandatory();
        validate_Area1_field_is_mandatory();
        validate_Pincode1_field_is_mandatory();
        validate_State_field_is_mandatory();
        validate_YearOFES_field_is_mandatory();


    }





}
