package PageObjects.ClientSetUp;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import PageObjects.BasePageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class AddClientPageObjects  extends BasePageObject{

    public AddClientPageObjects(WebDriver driver)


    {
        super(driver);
        left_menu_list.add(0,"Basic Details");
        left_menu_list.add(1,"Division Master Setup");
        left_menu_list.add(2,"Region Master Setup");
        left_menu_list.add(3,"Client Users Designation Master");
        left_menu_list.add(4,"Client Users Setup");
        left_menu_list.add(5,"Packaging");
        left_menu_list.add(6,"Brands");
        left_menu_list.add(7,"Category");
        left_menu_list.add(8,"Sub-category");
        left_menu_list.add(9,"Product");
        left_menu_list.add(10,"Business Entity Role Master");

    }
    public Map<String, String> q;
    ArrayList<String> left_menu_list = new ArrayList<String>();



    /*Left Menu*/

    @FindBy(css = "#clientSetupContainer > div > div.commonNavContainer.col-lg-2 > div > div > ul > li")
    public List<WebElement> Add_EditClientLeftMenu;

   /* BASIC DETAILS*/

    @FindBy(id = "clientCode")
    @CacheLookup
    public WebElement clientCode;

    @FindBy(xpath = "//label[contains(@for,'clientCode')]")
    public WebElement clientCode_label;

    @FindBy(id = "clientName")
    @CacheLookup
    public WebElement clientName;

    @FindBy(xpath = "//label[contains(@for,'clientCode')]")
    public WebElement clientName_label;

    @FindBy(id = "taxAccountNumber")
    @CacheLookup
    public WebElement taxAccountNumber;

    @FindBy(xpath = "//label[contains(@for,'taxAccountNumber')]")
    public WebElement taxAccountNumber_label;

    @FindBy(id = "salesTaxRegistrationNumber")
    @CacheLookup
    public WebElement salesTaxRegistrationNumber;

    @FindBy(xpath = "//label[contains(@for,'salesTaxRegistrationNumber')]")
    public WebElement salesTaxRegistrationNumber_label;


    /*UI INFORMATION*/

    @FindBy(css = "#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > div:nth-child(3) > div.form-group.row > file-upload-content:nth-child(1) > div > div >  input")
    @CacheLookup
    public WebElement clientLogo1;

    @FindBy(css = "#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > div:nth-child(3) > div.form-group.row > file-upload-content:nth-child(1) > div > label")
    public WebElement clientLogo1_label;

    @FindBy(css = "#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > div:nth-child(3) > div.form-group.row > file-upload-content:nth-child(2) > div > div > input")
    @CacheLookup
    public WebElement bannerImages1;

    @FindBy(css = "/#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > div:nth-child(3) > div.form-group.row > file-upload-content:nth-child(2) > div > label")
    @CacheLookup
    public WebElement bannerImages1_label;

    @FindBy(id = "clientBrandColor")
    @CacheLookup
    public WebElement brandColor1;

    @FindBy(xpath = "//label[contains(@for,'clientBrandColor')]")
    public WebElement clientBrandColor_label;

    /*SERVICE URLS*/

    @FindBy(id = "clientSetupUrl")
    @CacheLookup
    public WebElement clientSetupUrl;

    @FindBy(xpath = "//label[contains(@for,'clientSetupUrl')]")
    public WebElement clientSetupUrl_label;

    @FindBy(id = "programOperationsUrl")
    @CacheLookup
    public WebElement programOperationsUrl;

    @FindBy(xpath = "//label[contains(@for,'programOperationsUrl')]")
    public WebElement programOperationsUrl_label;

    @FindBy(id = "programSetupUrl")
    @CacheLookup
    public WebElement programSetupUrl;

    @FindBy(xpath = "//label[contains(@for,'programSetupUrl')]")
    public WebElement programSetupUrl_label;


    /*Office Address*/

    @FindBy(id = "registeredStreetAddress")
    @CacheLookup
    public WebElement officestreetAddress1;

    @FindBy(xpath = "//label[contains(@for,'registeredStreetAddress')]")
    public WebElement officestreetAddress1_label;

    @FindBy(id = "registeredArea")
    @CacheLookup
    public WebElement Officearea1Address;

    @FindBy(xpath = "//label[contains(@for,'registeredArea')]")
    public WebElement OfficeArea1_label;

    @FindBy(id = "registeredLandmark")
    @CacheLookup
    public WebElement Officelandmark1;

    @FindBy(xpath = "//label[contains(@for,'registeredLandmark')]")
    public WebElement Officelandmark1_label;

    @FindBy(id = "registeredState")
    @CacheLookup
    public WebElement Officestate1;

    @FindBy(xpath = "//input[@formcontrolname='registeredOfficeState']")
    @CacheLookup
    public WebElement Officestate1_editPage;

    @FindBy(xpath = "//label[contains(@for,'registeredState')]")
    public WebElement Officestate1_label;

    @FindBy(id = "registeredCity")
    @CacheLookup
    public WebElement Officecity1;

    @FindBy(xpath = "//input[contains(@formcontrolname,'registeredOfficeCity')]")
    @CacheLookup
    public WebElement Officecity1_editPage;

    @FindBy(xpath = "//label[contains(@for,'registeredOfficeCity')]")
    public WebElement OfficeCity1label;

    @FindBy(xpath = "//input[@formcontrolname='registeredOfficePinCode']")
    @CacheLookup
    public WebElement pincode1;

    @FindBy(xpath = "//label[contains(@for,'registeredPincode')]")
    public WebElement pincode1_label;

    /*COMMUNICATION ADDRESS*/

    @FindBy(id = "communicationStreetAddress")
    @CacheLookup
    public WebElement communicationStreetAddress;

    @FindBy(xpath = "//label[contains(@for,'communicationStreetAddress')]")
    public WebElement communicationStreetAddress_label;

    @FindBy(id = "communicationState")
    @CacheLookup
    public WebElement communicationState;


    @FindBy(xpath = "//label[contains(@for,'communicationState')]")
    public WebElement communicationState_label;

    @FindBy(id = "communicationLandmark")
    @CacheLookup
    public WebElement communicationLandmark;

    @FindBy(xpath = "//label[contains(@for,'communicationLandmark')]")
    public WebElement communicationLandmark_label;


    @FindBy(id = "communicationArea")
    @CacheLookup
    public WebElement communicationArea;

    @FindBy(xpath = "//label[contains(@for,'communicationArea')]")
    public WebElement communicationArea_label;

    @FindBy(id = "communicationCity")
    @CacheLookup
    public WebElement communicationCity;

    @FindBy(xpath = "//label[contains(@for,'communicationCity')]")
    public WebElement communicationCity_label;

    @FindBy(xpath= "//input[@formcontrolname=\"communicationPinCode\"]")
    @CacheLookup
    public WebElement communicationPincode;

    @FindBy(xpath = "//label[contains(@for,'communicationPincode')]")
    @CacheLookup
    public WebElement communicationPincode_label;


    /*CONTACT INFO 1*/


    @FindBy(id = "personName")
    @CacheLookup
    public WebElement contactPersonName;

    @FindBy(xpath = "//label[contains(@for,'personName')]")
    public WebElement contactPersonName_label;

    @FindBy(id = "mobileNumber")
    @CacheLookup
    public WebElement contact_mobileNumber;


    @FindBy(xpath = "//label[contains(@for,'mobileNumber')]")
    public WebElement contact_mobileNumber_label;

    @FindBy(id = "officeNumber")
    @CacheLookup
    public WebElement   contact_officeNumber;

    @FindBy(xpath = "//label[contains(@for,'officeNumber')]")
    public WebElement contact_officeNumber_label;

    @FindBy(css = "#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > div:nth-child(7) > div > div.ng-pristine.ng-invalid.ng-touched > div > div:nth-child(4) > div > input")
    @CacheLookup
    public WebElement contact_officeNumberEmail;

    @FindBy(css = "#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form > div.ng-untouched.ng-pristine.ng-valid > div > div.ng-untouched.ng-pristine.ng-valid > div > div:nth-child(4) > div > input")
    public WebElement contact_officeNumberEmailEditPage;

    @FindBy(xpath = "//label[contains(@for,'email')]")
    public WebElement contact_officeNumberEmail_label;

    @FindBy(css = "#clientSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component form:nth-of-type(1) div:nth-of-type(7) button.innerPageButtons")
    @CacheLookup
    public WebElement addContact;

    /* BRANCH INFO 1*/

    @FindBy(id = "branchName")
    @CacheLookup
    public WebElement branchName;

    @FindBy(xpath = "//label[contains(@for,'branchName')]")
    public WebElement branchName_label;


    @FindBy(id = "branchHeadName")
    @CacheLookup
    public WebElement branchHeadName;

    @FindBy(xpath = "//label[contains(@for,'branchHeadName')]")
    public WebElement branchHeadName_label;



    @FindBy(id = "contactNumber")
    @CacheLookup
    public WebElement branch_contactNumber;

    @FindBy(xpath = "//label[contains(@for,'contactNumber')]")
    public WebElement branch_contactNumber_label;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > div:nth-child(9) > div > div.ng-pristine.ng-invalid.ng-touched > div:nth-child(2) > div:nth-child(2) > div > input")
    @CacheLookup
    public WebElement branch_email2;

    @FindBy(xpath="//*[@id=\"clientSetupBodyContainer\"]/div/div[2]/ng-component/form/div[8]/div/div[2]/div[2]/div[2]/div/input")
    @CacheLookup
    public WebElement branch_email2Editpage;


    @FindBy(xpath = "//label[@for='email']")
    @CacheLookup
    public WebElement branch_email2_label;

    @FindBy(id = "streetAddress")
    @CacheLookup
    public WebElement branchInfo_streetAddress;

    @FindBy(xpath = "//label[contains(@for,'streetAddress')]")
    public WebElement branchInfo_streetAddress_label;

    @FindBy(id = "area")
    @CacheLookup
    public WebElement branch_area;

    @FindBy(xpath = "//label[contains(@for,'area')]")
    public WebElement branch_area_label;

    @FindBy(id = "landmark")
    @CacheLookup
    public WebElement branchlandmark3;

    @FindBy(xpath = "//label[contains(@for,'landmark')]")
    public WebElement branchlandmark3_label;

    @FindBy(id = "state")
    @CacheLookup
    public WebElement branch_state3;

    @FindBy(xpath = "//label[contains(@for,'state')]")
    @CacheLookup
    public WebElement branch_state3_label;

    @FindBy(id = "city")
    @CacheLookup
    public WebElement branch_city3;

    @FindBy(xpath = "//label[contains(@for,'city')]")
    public WebElement branch_city3_label;

    @FindBy(id = "pincode")
    @CacheLookup
    public WebElement branchPincode3;

    @FindBy(xpath = "//label[contains(@for,'pincode')]")
    @CacheLookup
    public WebElement branch_pincode3_label;

    @FindBy(css = "#clientSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(1) div.col-lg-3 button.innerPageButtons")
    @CacheLookup
    public WebElement addBranch;

    @FindBy(css = "#clientSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(2) div:nth-of-type(3) button.innerPageButtons")
    @CacheLookup
    public WebElement branch_reset;


    @FindBy(css = "#clientSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(2) div:nth-of-type(2) button.innerPageButtons")
    @CacheLookup
    public WebElement cancel1;

    @FindBy(css = "#clientSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component form:nth-of-type(2) div.row div:nth-of-type(5) div:nth-of-type(2) button.innerPageButtons")
    @CacheLookup
    public WebElement cancel2;



    /*//LeFt Menu Link*/

    @FindBy(css = "a[href='/clientSetup/clientList']")
    @CacheLookup
    public WebElement clientList;




    /*BreadCrumb Links*/

    @FindBy(css = "a[href='/clientSetup/basicdetails-add']")
    @CacheLookup
    public WebElement clientSetupAddBasicDetails;

    @FindBy(css = "a[href='/clientSetup']")
    @CacheLookup
    public WebElement clientsSetup;

    @FindBy(css = "a[href='/home']")
    @CacheLookup
    public WebElement home;




   /* ADMIN DETAILS*/

    @FindBy(id = "firstName")
    @CacheLookup
    public WebElement admin_firstName;

    @FindBy(xpath = "//label[contains(@for,'firstName')]")
    public WebElement admin_firstName_label;

    @FindBy(id = "lastName")
    @CacheLookup
    public WebElement admin_lastName;

    @FindBy(xpath = "//label[contains(@for,'lastName')]")
    public WebElement admin_lastName_label;

    @FindBy(xpath="//input[@formcontrolname=\"emailId\"]")
    @CacheLookup
    public WebElement admin_email3;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form.ng-pristine.ng-invalid.ng-touched > div > div:nth-child(3) > div:nth-child(1) > label")
    @CacheLookup
    public WebElement admin_email3_label;

    @FindBy(id = "mobileNumber")
    @CacheLookup
    public WebElement admin_mobileNumber2;

    @FindBy(xpath = "//label[contains(@for,'mobileNumber')]")
    public WebElement admin_mobileNumber2_label;


    @FindBy(id = "password")
    @CacheLookup
    public WebElement admin_password;

    @FindBy(xpath = "//label[contains(@for,'password')]")
    public WebElement admin_password_label;

    @FindBy(css = "#clientSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component form:nth-of-type(2) div.row div:nth-of-type(5) div:nth-of-type(3) button.innerPageButtons")
    @CacheLookup
    public WebElement admin_reset;



    @FindBy(id = "logoutBt")
    @CacheLookup
    public WebElement logout;


    public final String pageLoadedText = "Client Setup - Add Basic Details";

    public final String pageUrl = "/clientSetup/basicdetails-add";


    @FindBy(css = "#clientSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component form:nth-of-type(2) div.row div:nth-of-type(5) div:nth-of-type(1) button.innerPageButtons")
    @CacheLookup
    public WebElement submit2;



    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > div:nth-child(3) > div:nth-child(1) > button")
    @CacheLookup
    public WebElement submit1;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > feedback-message > div > div:nth-child(3) > div > a")
    public WebElement FeedbackAlert;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > form:nth-child(1) > feedback-message > div > div:nth-child(3) > div > div")
    public WebElement FeedbackAlert1;

    @FindBy(xpath = "//div[contains(@class, 'alert alert-danger inputValidationErr ')]")
    public List<WebElement> ClientDetails_page_alert;

     /* Footer Links*/

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(1) a")
    @CacheLookup
    public WebElement about;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(3) a")
    @CacheLookup
    public WebElement termsOfUse;


    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(4) a")
    @CacheLookup
    public WebElement faq;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(2) a")
    @CacheLookup
    public WebElement privacy;




    public AddClientPageObjects(WebDriver driver, Map<String, String> data) {
        this(driver);

    }



    /**
     * Click on About Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickAboutLink() {
        about.click();
        return this;
    }

    /**
     * Click on Add Branch Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickAddBranchButton() {
        addBranch.click();
        return this;
    }

    /**
     * Click on Add Contact Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickAddContactButton() {
        addContact.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickCancel1Button() {
        cancel1.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickCancel2Button() {
        cancel2.click();
        return this;
    }

    /**
     * Click on Client List Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickClientListLink() {
        clientList.click();
        return this;
    }

    /**
     * Click on Client Setup Add Basic Details Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickClientSetupAddBasicDetailsLink() {
        clientSetupAddBasicDetails.click();
        return this;
    }

    /**
     * Click on Clients Setup Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickClientsSetupLink() {
        clientsSetup.click();
        return this;
    }

    /**
     * Click on Faq Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickFaqLink() {
        faq.click();
        return this;
    }

    /**
     * Click on Home Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickHomeLink() {
        home.click();
        return this;
    }

    /**
     * Click on Logout Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickLogoutLink() {
        logout.click();
        return this;
    }

    /**
     * Click on Privacy Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickPrivacyLink() {
        privacy.click();
        return this;
    }

    /**
     * Click on Reset Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickReset1Button() {
        branch_reset.click();
        return this;
    }

    /**
     * Click on Reset Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickReset2Button() {
        branch_reset.click();
        return this;
    }

    /**
     * Click on Submit Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickSubmit1Button() {
        submit1.click();
        return this;
    }

    /**
     * Click on Submit Button.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickSubmit2Button() {
        submit2.click();
        return this;
    }

    /**
     * Click on Terms Of Use Link.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects clickTermsOfUseLink() {
        termsOfUse.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the Client_setup class instance.
     * @param Client_Name
     * @param Tax_Account_Number
     * @param sales_tax
     * @param Client_logo
     * @param Banner_Images

     * @param Program_Operations_Url
     * @param Street_Address1
     * @param Area1
     * @param Landmark1
     * @param State1
     * @param city1
     * @param pinc1
     * @param Street_Address2
     * @param Area2
     * @param Landmark3
     * @param State3
     * @param city3
     * @param pinc3
     * @param Person_Name
     * @param Mobile
     * @param Office
     * @param Email
     * @param EmaiLbranch
     * @param Branch_head
     * @param Contact_Number
     * @param State2
     * @param Street_Address3
     * @param Area3
     * @param Landmark2

     */
    public AddClientPageObjects fill(String ClientCode,String Client_Name,	String Tax_Account_Number,	String sales_tax,
                                     String Client_logo,	String Banner_Images,	String Brand_Color,	String Program_Setup_Url,
                                     String Program_Operations_Url,	String Street_Address1,	String Area1,	String Landmark1,
                                     String State1,	String city1,	String pinc1,	String Street_Address2,	String Area2,
                                     String Landmark2,	String State2,	String city2,	String pinc2,	String Person_Name,
                                     String Mobile,	String Office,	String Email,	String Branch,	String Branch_head,
                                     String Contact_Number,	String  EmaiLbranch,	String Street_Address3,	String Area3,
                                     String Landmark3,	String State3,	String city3,	String pinc3,
                                     String Fname,String lname,String email_admin,String Admin_mobile,String password) throws InterruptedException, AWTException {


        setClientCodeTextField(ClientCode);
        setClientNameTextField(Client_Name);
        setTaxAccountNumberTextField(Tax_Account_Number);
        setSalesTaxRegistrationNumberTextField(sales_tax);
        setClientLogo1FileField(Client_logo);
        setBannerImages1FileField(Banner_Images);
        setBrandColor1TextField(Brand_Color);
        setProgramSetupUrlTextField(Program_Setup_Url);
        setProgramOperationsUrlTextField(Program_Operations_Url);
        setStreetAddress1TextField(Street_Address1);
        setArea1TextField(Area1);
        setLandmark1TextField(Landmark1);
        setState1DropDownListField(State1);
        setCity1DropDownListField(city1);
        setPincode1TextField(pinc1);
        setStreetAddress2TextField(Street_Address2);
        setArea2TextField(Area2);
        setLandmark2TextField(Landmark2);
        setState2DropDownListField(State2);
        setCity2DropDownListField(city2);
        setPincode2TextField(pinc2);
        setPersonNameTextField(Person_Name);
        setMobileNumber1TextField(Mobile);
        setOfficeNumberTextField(Office);
        setEmail1TextField(Email);
        setBranchNameTextField(Branch);
        setBranchHeadNameTextField(Branch_head);
        setContactNumberTextField(Contact_Number);
        setEmail2TextField(EmaiLbranch);
        setStreetAddress3TextField(Street_Address3);
        setArea3TextField(Area3);
        setLandmark3TextField(Landmark3);
        setState3DropDownListField(State3);
        setCity3DropDownListField(city3);
        setPincode3TextField(pinc3);
      /*  setFirstNameTextField(Fname);
        setLastNameTextField(lname);
        setEmail3TextField(email_admin);
        setMobileNumber2TextField(Admin_mobile);
        setPasswordTextField(password);*/
        return this;
    }



    public AddClientPageObjects fillAndSubmit() {

       waitForElementClickableByElement(submit1);
     //  waitForElementByElement(submit2);
       submit1.click();
       return this;
    }

    /**
     * Set default value to Area Text field.
     *
     * @return the Client_setup class instance.
     */


    /**
     * Set value to Area Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setArea1TextField(String area1Value) {

        waitForElementByElement(Officearea1Address);
        Officearea1Address.sendKeys(area1Value);
        return this;
    }

    public AddClientPageObjects ValidateArea1TextField()
    {
        validate_field_is_mandatory(Officearea1Address, OfficeArea1_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Area Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setArea2TextField(String area2Value) {
        waitForElementByElement(communicationArea);
        communicationArea.sendKeys(area2Value);
        return this;
    }

    public AddClientPageObjects ValidateComunicationArea2TextField()
    {
        validate_field_is_mandatory(communicationArea,communicationArea_label,ClientDetails_page_alert);
        return this;
    }


    /**
     * Set value to Area Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setArea3TextField(String area3Value) {

        waitForElementByElement(branch_area);
        branch_area.sendKeys(area3Value);
        return this;
    }

    public AddClientPageObjects ValidateBranchArea3TextField()
    {
        validate_field_is_mandatory(branch_area, branch_area_label,ClientDetails_page_alert);
        return this;
    }


    /**
     * Set Banner Images File field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setBannerImages1FileField(String bannerImagesValue) {
        final String pathDir = new java.io.File("").getAbsolutePath();
        final String pathFile = pathDir + "\\banner.png";
        waitForElementByElement(bannerImages1);
        bannerImages1.sendKeys(pathFile);
        return this;
    }

    /**
     * validate Banner Images File field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects ValidateBannerImages1field()
    {
        validate_field_is_mandatory(bannerImages1,bannerImages1_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set Banner Images File field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setBannerImages2FileField() {
        return this;
    }



    /**
     * Set value to Branch Head Name Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setBranchHeadNameTextField(String branchHeadNameValue) {
        branchHeadName.sendKeys(branchHeadNameValue);
        return this;
    }

    /**
     * Validate Branch head Name Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateBranchHeadNamefield()
    {
        validate_field_is_mandatory(branchHeadName,branchHeadName_label,ClientDetails_page_alert);
        return this;
    }






    /**
     * Set value to Branch Name Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setBranchNameTextField(String branchNameValue) {
        branchName.sendKeys(branchNameValue);
        return this;
    }
    ;
    /**
     * Validate Branch Name Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateBranchNamefield()
    {
        validate_field_is_mandatory(branchName,branchName_label,ClientDetails_page_alert);
        return this;
    }
    /**
     * Set default value to Brand Color Text field.
     *
     * @return the Client_setup class instance.
     */


    public AddClientPageObjects setBrandColor1TextField(String brandColor1Value) throws InterruptedException, AWTException {



        JavascriptExecutor js = (JavascriptExecutor) driver;
        String sampleJS = "document.getElementById('clientBrandColor').value='#400040'";
        js.executeScript(sampleJS);
        Actions action = new Actions(driver);
        brandColor1.click();
        Thread.sleep(1000);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_LEFT);
        robot.keyPress(KeyEvent.VK_SPACE);
        robot.keyPress(KeyEvent.VK_ENTER);
        return this;
    }

    /**
     * Validate Brand Color Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateBrandColorField()
    {
        validate_field_is_mandatory(brandColor1,clientBrandColor_label,ClientDetails_page_alert);
        return this;
    }




    /**
     * Set default value to City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */


    /**
     * Set value to City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setCity1DropDownListField(String city1Value) {
        new Select(Officecity1).selectByVisibleText(city1Value);
        return this;
    }

    /**
     * Set default value to City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */


    /**
     * Set value to City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setCity2DropDownListField(String city2Value) {
        new Select(communicationCity).selectByVisibleText(city2Value);
        return this;
    }

    /**
     * Set default value to City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */


    /**
     * Set value to City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setCity3DropDownListField(String city3Value) {
        new Select(branch_city3).selectByVisibleText(city3Value);
        return this;
    }
    /**
     * Validate City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateBranchCity3DropDownListField()
    {

        validate_field_is_mandatory(branch_city3,branch_city3_label,ClientDetails_page_alert);
        return this;
    }

    /**
     * Set default value to Client Code Text field.
     *
     * @return the Client_setup class instance.
     */

    /**
     * Verify Client Code Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateClientCodeTextField() {

        validate_field_is_mandatory(clientCode,clientCode_label,ClientDetails_page_alert);
        return this;
    }
    /**
     * Verify Client Code Name field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateClientNameTextField() {

        validate_field_is_mandatory(clientName,clientName_label,ClientDetails_page_alert);
        return this;
    }
    /**
     * Set value to Client Code Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setClientCodeTextField(String clientCodeValue) {

        waitForElementByElement(clientCode);
        clientCode.sendKeys(clientCodeValue);
        return this;
    }

    /**
     * Set default value to Client Logo Text field.
     *
     * @return the Client_setup class instance.
     */


    /**
     * Set Client Logo File field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setClientLogo1FileField(String clientLogoValue) {

        final String pathDir = new java.io.File("").getAbsolutePath();
        final String pathFile = pathDir + "\\logo.jpg";
        clientLogo1.sendKeys(pathFile);
        return this;
    }

    public AddClientPageObjects ValidateClientLogoield()
    {
        validate_field_is_mandatory(clientLogo1,clientLogo1_label,ClientDetails_page_alert);
        return this;
    }

    /**
     * Set Client Logo File field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setClientLogo2FileField() {
        return this;
    }

    /**
     * Set default value to Client Name Text field.
     *
     * @return the Client_setup class instance.
     */


    /**
     * Set value to Client Name Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setClientNameTextField(String clientNameValue) {
        clientName.sendKeys(clientNameValue);
        return this;
    }

    /**
     * Set value to Tax Account Number Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setTaxAccountNumberTextField(String taxAccountNumberValue) {
        taxAccountNumber.sendKeys(taxAccountNumberValue);
        return this;
    }

    public AddClientPageObjects ValidateTaxAccountNumberTextField()
    {
        validate_field_is_mandatory(taxAccountNumber,taxAccountNumber_label,ClientDetails_page_alert);
        return this;
    }

    /**
     * Set default value to Client Setup Url Text field.
     *
     * @return the Client_setup class instance.
     */

    /**
     * Set value to Client Setup Url Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setClientSetupUrlTextField(String clientSetupUrlValue) {
        clientSetupUrl.sendKeys(clientSetupUrlValue);
        return this;
    }
    /**
     * Validate Client Setup Url   field.
     *
     * @return the Client_setup class instance.
     */
   /* public AddClientPageObjects ValidateClientSetUpfield()
    {
        validate_field_is_mandatory(clientSetupUrl,clientSetupUrl_label,ClientDetails_page_alert);
        return this;
    }
*/
    public AddClientPageObjects ValidatestreetAddress1field()
    {
        validate_field_is_mandatory(officestreetAddress1, officestreetAddress1_label,ClientDetails_page_alert);
        return this;
    }

    public AddClientPageObjects ValidateOfficeLandamarkfield()
    {
        validate_field_is_mandatory(Officelandmark1, Officelandmark1_label,ClientDetails_page_alert);
        return this;
    }

    public AddClientPageObjects ValidateOfficeStatefield()
    {
        validate_field_is_mandatory(Officestate1, Officestate1_label,ClientDetails_page_alert);
        return this;
    }

    public AddClientPageObjects ValidateOfficeCityfield()
    {
        validate_field_is_mandatory(Officecity1, OfficeCity1label,ClientDetails_page_alert);
        return this;
    }


    public AddClientPageObjects ValidateOfficePincodefield()
    {
        validate_field_is_mandatory(pincode1,pincode1_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set default value to Contact Number Text field.
     *
     * @return the Client_setup class instance.
     */


    /**
     * Set value to Contact Number Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setContactNumberTextField(String contactNumberValue) {
        branch_contactNumber.sendKeys(contactNumberValue);
        return this;
    }

    /**
     * Validate Branch Contact Number Text field.
     *
     * @return the Client_setup class instance.
     */


    public AddClientPageObjects ValidateBranchContactNumberfield()
    {
        getElementByText(ClientDetails_page_alert,"Mobile Number is required");
      //  validate_field_is_mandatory(branch_contactNumber,branch_contactNumber_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Email Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setEmail1TextField(String email1Value) {

        waitForElementByBy(By.xpath("//input[@class='form-control ng-untouched ng-pristine ng-invalid' and @id='email']")).sendKeys(email1Value);
       // contact_officeNumberEmail.sendKeys(email1Value);
        return this;
    }

    public AddClientPageObjects ValidateContactOfficeEmailfield()
    {
        validate_field_is_mandatory(contact_officeNumberEmail, contact_officeNumberEmail_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Email Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects setEmail2TextField(String email2Value) {
       // branch_email2.sendKeys(email2Value);
        waitForElementByBy(By.xpath("//input[@class='form-control ng-untouched ng-pristine ng-invalid' and @id='email']")).sendKeys(email2Value);
        return this;
    }

    /**
     * Validate Branch   Email Text field.
     *
     * @return the Client_setup class instance.
     */


    public AddClientPageObjects ValidateBranchEmail( )
    {
        validate_field_is_mandatory(branch_email2,branch_email2_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Email Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setEmail3TextField(String email3Value) {
        admin_email3.sendKeys(email3Value);
        return this;
    }



    /**
     * Set value to First Name Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setFirstNameTextField(String firstNameValue) {
        admin_firstName.sendKeys(firstNameValue);
        return this;
    }

    public AddClientPageObjects ValidateFirstNameFieldAdmin()
    {
        validate_field_is_mandatory(admin_firstName,admin_firstName_label,ClientDetails_page_alert);
        return this ;
    }

    public AddClientPageObjects ValidateLastNameFieldAdmin()
    {
        validate_field_is_mandatory(admin_lastName,admin_lastName_label,ClientDetails_page_alert);
        return this ;
    }

    public AddClientPageObjects ValidateEmailFieldAdmin()
    {
        validate_field_is_mandatory(admin_email3,admin_email3_label,ClientDetails_page_alert);
        return this ;
    }




    /**
     * Set value to Landmark Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setLandmark1TextField(String landmark1Value) {
        Officelandmark1.sendKeys(landmark1Value);
        return this;
    }



    /**
     * Set value to Landmark Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setLandmark2TextField(String landmark2Value) {
        communicationLandmark.sendKeys(landmark2Value);
        return this;
    }



    /**
     * Set value to Landmark Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setLandmark3TextField(String landmark3Value) {
        branchlandmark3.sendKeys(landmark3Value);
        return this;
    }


    /**
     * Validate Landmark Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateLandmark3TextField( ) {

        validate_field_is_mandatory(branchlandmark3,branchlandmark3_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Last Name Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setLastNameTextField(String lastNameValue) {
        admin_lastName.sendKeys(lastNameValue);
        return this;
    }



    /**
     * Set value to Mobile Number Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setMobileNumber1TextField(String mobileNumber1Value) {
        contact_mobileNumber.sendKeys(mobileNumber1Value);
        return this;
    }

    public AddClientPageObjects ValidateContactMobileField()
    {
        validate_field_is_mandatory(contact_mobileNumber,contact_mobileNumber_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Mobile Number Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setAdminMobileNumber2TextField(String mobileNumber2Value) {
        admin_mobileNumber2.sendKeys(mobileNumber2Value);
        return this;
    }

    /**
     * Validate Admin Mobile Number Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects ValidateAdminMobileNumber2TextField() {
        validate_field_is_mandatory(admin_mobileNumber2,admin_mobileNumber2_label,ClientDetails_page_alert);
        return this;
    }




    /**
     * Set value to Office Number Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setOfficeNumberTextField(String officeNumberValue) {
        contact_officeNumber.sendKeys(officeNumberValue);

        contact_mobileNumber.sendKeys(Keys.TAB);
        return this;
    }


    public AddClientPageObjects ValidateContactOfficeNumberField()
    {
        validate_field_is_mandatory(contact_officeNumber,contact_officeNumber_label,ClientDetails_page_alert);
        return this;
    }
    /**
     * Set value to Password Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setMobileNumber2TextField(String mobileNumber2TextField) {

        waitForElementByBy(By.xpath("//input[@class='form-control ng-untouched ng-pristine ng-invalid' and @id='mobileNumber']")).sendKeys(mobileNumber2TextField);

       // admin_mobileNumber2.sendKeys(mobileNumber2TextField);
        return this;
    }



    /**
     * Set value to Password Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setPasswordTextField(String passwordValue) {
        admin_password.sendKeys(passwordValue);
        return this;
    }


    /**
     * Set value to Password Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects ValidateAdminPasswordTextField( ) {
        validate_field_is_mandatory(admin_password,admin_password_label,ClientDetails_page_alert);
        return this;
    }


    /**
     * Set value to Person Name Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setPersonNameTextField(String personNameValue) {
        contactPersonName.sendKeys(personNameValue);
        return this;
    }

    /**
     * Validate Person Name Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects ValidatePersonNameTextField( ) {

        validate_field_is_mandatory(contactPersonName,contactPersonName_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Pincode Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setPincode1TextField(String pincode1Value) {
        pincode1.sendKeys(pincode1Value);
        return this;
    }



    /**
     * Set value to Pincode Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setPincode2TextField(String pincode2Value) {
        communicationPincode.sendKeys(pincode2Value);
        return this;
    }


    /**
     * Set value to Pincode Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects setPincode3TextField(String pincode3Value) {
        branchPincode3.sendKeys(pincode3Value);
        return this;
    }

    /**
     *Validate  Pincode Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects ValidateBranchPincode3TextField( ) {

       validate_field_is_mandatory(branchPincode3,branch_pincode3_label,ClientDetails_page_alert);
        return this;
    }




    /**
     * Set value to Program Operations Url Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setProgramOperationsUrlTextField(String programOperationsUrlValue) {
        programOperationsUrl.sendKeys(programOperationsUrlValue);
        return this;
    }

    /**
     * Validate Program Setup Url Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateProgramOperationsUrlfield()
    {
        validate_field_is_mandatory(programOperationsUrl,programOperationsUrl_label,ClientDetails_page_alert);
        return this;
    }





    /**
     * Set value to Program Setup Url Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setProgramSetupUrlTextField(String programSetupUrlValue) {
        programSetupUrl.sendKeys(programSetupUrlValue);
        return this;
    }

    /**
     * Validate Program Setup Url Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects ValidateProgramSetupUrlField()
    {
        validate_field_is_mandatory(programSetupUrl,programSetupUrl_label,ClientDetails_page_alert);
        return this;
    }



    /**
     * Set value to Sales Tax Registration Number Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setSalesTaxRegistrationNumberTextField(String salesTaxRegistrationNumberValue) {
        salesTaxRegistrationNumber.sendKeys(salesTaxRegistrationNumberValue);
        return this;
    }



    public AddClientPageObjects ValidateSalesTaxfield()
    {
        validate_field_is_mandatory(salesTaxRegistrationNumber,salesTaxRegistrationNumber_label,ClientDetails_page_alert);
        return this;
    }


    public AddClientPageObjects ValidateCommunicationStreetAddress_field()
    {
        validate_field_is_mandatory(communicationStreetAddress,communicationStreetAddress_label,ClientDetails_page_alert);
        return this;
    }

    public AddClientPageObjects ValidateCommunicationState_field()
    {
        validate_field_is_mandatory(communicationState,communicationState_label,ClientDetails_page_alert);
        return this;
    }

    public AddClientPageObjects ValidateCommunicationLandmark_field()
    {
        validate_field_is_mandatory(communicationLandmark,communicationLandmark_label,ClientDetails_page_alert);
        return this;
    }

    public AddClientPageObjects ValidateCommunicationCity_field()
    {
        validate_field_is_mandatory(communicationCity,communicationCity_label,ClientDetails_page_alert);
        return this;
    }

    public AddClientPageObjects ValidateCommunicationPincode_field()
    {
        validate_field_is_mandatory(communicationPincode,communicationPincode_label,ClientDetails_page_alert);
        return this;
    }




    /**
     * Set value to State Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setState1DropDownListField(String state1Value) {
        new Select(Officestate1).selectByVisibleText(state1Value);
        return this;
    }



    /**
     * Set value to State Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setState2DropDownListField(String state2Value) {
        new Select(communicationState).selectByVisibleText(state2Value);
        return this;
    }



    /**
     * Set value to State Drop Down List field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects setState3DropDownListField(String state3Value) {
        new Select(branch_state3).selectByVisibleText(state3Value);
        return this;
    }

    /**
     * Validate  State Drop Down List field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects setState3DropDownListField( ) {
        validate_field_is_mandatory(branch_state3,branch_state3_label,ClientDetails_page_alert);
        return this;
    }


    /**
     * Set value to Street Address Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setStreetAddress1TextField(String streetAddress1Value) {
        officestreetAddress1.sendKeys(streetAddress1Value);
        return this;
    }



    /**
     * Set value to Street Address Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setStreetAddress2TextField(String streetAddress2Value) {
        communicationStreetAddress.sendKeys(streetAddress2Value);
        return this;
    }



    /**
     * Set value to Street Address Text field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects setStreetAddress3TextField(String streetAddress3Value) {
        branchInfo_streetAddress.sendKeys(streetAddress3Value);
        return this;
    }

    /**
     * Validate  Street Address Text field.
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects validatebranchStreetAddress3TextField( ) {
        validate_field_is_mandatory(branchInfo_streetAddress,branchInfo_streetAddress_label,ClientDetails_page_alert);
        return this;
    }





    /**
     * Submit the form to target page.
     *
     * @return the Client_setup class instance.
     */
    /*public AddClientPageObjects submit() {
        clickAddContactButton();
        AddClientPageObjects target = new AddClientPageObjects(driver, data, 15);
        PageFactory.initElements(driver, target);
        return target;
    }*/



    /**
     * Unset value from City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects unsetCity1DropDownListField(String city1Value) {
        new Select(Officecity1).deselectByVisibleText(city1Value);
        return this;
    }



    /**
     * Unset value from City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects unsetCity2DropDownListField(String city2Value) {
        new Select(communicationCity).deselectByVisibleText(city2Value);
        return this;
    }



    /**
     * Unset value from City Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects unsetCity3DropDownListField(String city3Value) {
        new Select(branch_city3).deselectByVisibleText(city3Value);
        return this;
    }



    /**
     * Unset value from State Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects unsetState1DropDownListField(String state1Value) {
        new Select(Officestate1).deselectByVisibleText(state1Value);
        return this;
    }



    /**
     * Unset value from State Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects unsetState2DropDownListField(String state2Value) {
        new Select(communicationState).deselectByVisibleText(state2Value);
        return this;
    }



    /**
     * Unset value from State Drop Down List field.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects unsetState3DropDownListField(String state3Value) {
        new Select(branch_state3_label).deselectByVisibleText(state3Value);
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects verifyPageLoaded() {
        (new WebDriverWait(driver, 15)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the Client_setup class instance.
     */
    public AddClientPageObjects verifyPageUrl() {
        (new WebDriverWait(driver, 15)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }


    public String verifyClientAddedAlert()

    {
        waitForElementByElement(FeedbackAlert);
        FeedbackAlert.click();
        waitForElementByElement(FeedbackAlert1);

        //waitForTextToBePresentInElementByElement(FeedbackAlert1,"Client record successfully created");
        return FeedbackAlert1.getText();
    }
    /**
     * Verify that Left Menu Shows all the Menu
     *
     * @return the Client_setup class instance.
     */

    public AddClientPageObjects verifyLeftMenu()

    {
     for(int i=0;i<left_menu_list.size();i++)
     {
         getElementByText(Add_EditClientLeftMenu,left_menu_list.get(i));
     }
    // getElementByText(Add_EditClientLeftMenu,"Business Entity Role Master");
        return this;
    }

    public AddClientPageObjects OpenDivisonMasterPage()
    {
        waitForElementByElement(Add_EditClientLeftMenu.get(1));
        Add_EditClientLeftMenu.get(1).click();
        return this;
    }

    public void Validate_all_fields_in_AddClientPage()
    {
        ValidateClientCodeTextField();
        ValidateClientNameTextField();
        ValidateTaxAccountNumberTextField();
        ValidateSalesTaxfield();
        ValidateBrandColorField();
        ValidateProgramOperationsUrlfield();
        ValidatestreetAddress1field();
        ValidateOfficeLandamarkfield();
        ValidateOfficeStatefield();
        ValidateOfficeCityfield();
        ValidateOfficePincodefield();
        ValidateCommunicationStreetAddress_field();
        ValidateComunicationArea2TextField();
        ValidateCommunicationState_field();
        ValidateCommunicationLandmark_field();
        ValidateCommunicationCity_field();
        ValidateCommunicationPincode_field();
        ValidatePersonNameTextField();
        ValidateContactMobileField();
        ValidateContactOfficeNumberField();
        ValidateContactOfficeEmailfield();
        ValidateBranchNamefield();
        ValidateBranchHeadNamefield();
        ValidateBranchContactNumberfield();
        ValidateBranchEmail( );
        validatebranchStreetAddress3TextField();
        ValidateBranchArea3TextField();
        ValidateLandmark3TextField( );
        ValidateBranchCity3DropDownListField();
        ValidateBranchPincode3TextField();
        ValidateFirstNameFieldAdmin();
        ValidateLastNameFieldAdmin();
        ValidateEmailFieldAdmin();
        ValidateAdminMobileNumber2TextField();
        ValidateAdminPasswordTextField();
    }

    public void verify_the_added_client_data(String client_Code, String client_Name, String tax_Account_Number,
                                             String sales_Tax_Registration_Number, String client_Images,
                                             String banner_Images, String brand_Color, String program_Setup_Url,
                                             String program_Operations_Url, String street_Address_OfficeAddress,
                                             String area_OfficeAddress, String landmark_OfficeAddress, String state_OfficeAddress,
                                             String city_OfficeAddress, String pincode_OfficeAddress, String street_Address_CommunicationAddress,
                                             String area_CommunicationAddress, String landmark_CommunicationAddress, String state_CommunicationAddress,
                                             String city_CommunicationAddress, String pincode_CommunicationAddress, String person_Name_Contact,
                                             String mobile_Number_Contact, String office_Number_Contact, String email_Contact, String bname,
                                             String bheadname, String branchContactNumber, String branchEmail2, String street_Address_Branch,
                                             String area_Branch, String landmark_Branch, String state_Branch, String city_Branch, String pincode_Branch,
                                             String fname, String lname, String email_admin, String admin_mobile, String password)
    {
        Assert.assertEquals(client_Code,getAttributeValue(clientCode,"value"));
        Assert.assertEquals(client_Name,getAttributeValue(clientName,"value"));
        Assert.assertEquals(tax_Account_Number,getAttributeValue(taxAccountNumber,"value"));
        Assert.assertEquals(sales_Tax_Registration_Number,getAttributeValue(salesTaxRegistrationNumber,"value"));
//        Assert.assertEquals(client_Images,getAttributeValue(clientLogo1,"value"));
       // Assert.assertEquals(banner_Images,getAttributeValue(bannerImages1,"value"));
        Assert.assertEquals(program_Setup_Url,getAttributeValue(programSetupUrl,"value"));
        Assert.assertEquals(program_Operations_Url,getAttributeValue(programOperationsUrl,"value"));
        Assert.assertEquals(street_Address_OfficeAddress,getAttributeValue(officestreetAddress1,"value"));
        Assert.assertEquals(area_OfficeAddress,getAttributeValue(Officearea1Address,"value"));
        Assert.assertEquals(landmark_OfficeAddress,getAttributeValue(Officelandmark1,"value"));
        Assert.assertEquals(state_OfficeAddress,getAttributeValue(Officestate1_editPage,"value"));
        Assert.assertEquals(city_OfficeAddress,getAttributeValue(Officecity1_editPage,"value"));
        Assert.assertEquals(street_Address_CommunicationAddress,getAttributeValue(communicationStreetAddress,"value"));
        Assert.assertEquals(area_CommunicationAddress,getAttributeValue(communicationArea,"value"));
        Assert.assertEquals(landmark_CommunicationAddress,getAttributeValue(communicationLandmark,"value"));
        Assert.assertEquals(state_CommunicationAddress,getAttributeValue(communicationState,"value"));
        Assert.assertEquals(city_CommunicationAddress,getAttributeValue(communicationCity,"value"));
        Assert.assertEquals(pincode_CommunicationAddress,getAttributeValue(communicationPincode,"value"));
        Assert.assertEquals(person_Name_Contact,getAttributeValue(contactPersonName,"value"));
        Assert.assertEquals(mobile_Number_Contact,getAttributeValue(contact_mobileNumber,"value"));
        Assert.assertEquals(office_Number_Contact,getAttributeValue(contact_officeNumber,"value"));
        Assert.assertEquals(email_Contact,getAttributeValue(contact_officeNumberEmailEditPage,"value"));
        Assert.assertEquals(bname,getAttributeValue(branchName,"value"));
        Assert.assertEquals(bheadname,getAttributeValue(branchHeadName,"value"));
        Assert.assertEquals(branchContactNumber,getAttributeValue(branch_contactNumber,"value"));
        Assert.assertEquals(branchEmail2,getAttributeValue(branch_email2Editpage,"value"));
        Assert.assertEquals(street_Address_Branch,getAttributeValue(branchInfo_streetAddress,"value"));
        Assert.assertEquals(area_Branch,getAttributeValue(branch_area,"value"));
        Assert.assertEquals(landmark_Branch,getAttributeValue(branchlandmark3,"value"));
        Assert.assertEquals(state_Branch,getAttributeValue(branch_state3,"value"));
        Assert.assertEquals(city_Branch,getAttributeValue(branch_city3,"value"));
        Assert.assertEquals(pincode_Branch,getAttributeValue(branchPincode3,"value"));
        /*Admin Assertion*/
        /*Assert.assertEquals(fname,getAttributeValue(admin_firstName,"value"));
        Assert.assertEquals(lname,getAttributeValue(admin_lastName,"value"));
        Assert.assertEquals(email_admin,getAttributeValue(admin_email3,"value"));
        Assert.assertEquals(admin_mobile,getAttributeValue(admin_mobileNumber2,"value"));
        Assert.assertEquals(password,getAttributeValue(admin_password,"value"));*/



    }

}
