package PageObjects.ClientSetUp;

import Common.Logging.PageObjectLogging;
import PageObjects.BasePageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Created by praveenk on 16-Mar-17.
 */
public class ClientUserSetupObjects extends BasePageObject{

    public ClientUserSetupObjects(WebDriver driver) {
        super(driver);
    }

    private final String pageUrl = "clientSetup/clientUsersSetup";

    private final String breadCrumbHomeLink="/home";

    private final String breadcrumbClientSetupLink = "/clientSetup";

    private final String breadcrumbClientUserSetupListLink = "/clientSetup/clientUsersSetup";

    private final String strPageTitle ="Clients Setup";

    @FindBy(id="appTitle")
    public WebElement pageTitle;

    @FindBy(css = "a[href='/home']")
    @CacheLookup
    public WebElement breadcrumbHome;

    @FindBy(css="a[href='/clientSetup']")
    @CacheLookup
    public WebElement breadcrumbClientSetup;

    @FindBy(css="a[href='/clientSetup/clientUsersSetup']")
    @CacheLookup
    public WebElement breadcrumbClientUserSetupList;

    public void verify_ClientUserSetupPageURL()
    {
        verifyURL(return_baseURL()+pageUrl);
        PageObjectLogging.log("verify_ClientUserSetupPageURL","Check the URL of Clients User Setup",true,driver);


    }

    public void verify_client_user_setup_page_title()
    {
        verifyPageHeading(strPageTitle, pageTitle);

    }

    public void verifyHomeBCLink()
    {

        VerifyBreadCrumbLinks(return_baseURL()+breadCrumbHomeLink,breadcrumbHome);
    }

    public void verifyClientSetupBCLink()
    {
        VerifyBreadCrumbLinks(return_baseURL()+breadcrumbClientSetupLink, breadcrumbClientSetup);
    }
    public void verifyClientUserSetupBCLink()
    {
        VerifyBreadCrumbLinks(return_baseURL()+ breadcrumbClientUserSetupListLink, breadcrumbClientUserSetupList);
    }

    public void verify_all_breadcrumb_links()
    {
        verifyClientSetupBCLink();
        verifyClientUserSetupBCLink();
        verifyHomeBCLink();

    }


}
