package PageObjects.ClientSetUp;

import Common.Logging.PageObjectLogging;
import PageObjects.BasePageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Created by praveenk on 10-Mar-17.
 */
public class DivisionMasterSetupPageObjects extends BasePageObject {

    public DivisionMasterSetupPageObjects(WebDriver driver) {
        super(driver);
    }

    private final String pageUrl = "clientSetup/divisionmastersetup";

    private final String breadCrumbHomeLink="/home";

    private final String breadcrumbClientListLink = "/clientSetup/clientList";

    private final String breadcrumbClientSetupDivisionMasterLink = "/clientSetup/divisionmastersetup";

    private final String strPageTitle ="Clients Setup";

    @FindBy(id="appTitle")
    public WebElement pageTitle;

    @FindBy(css = "a[href='/home']")
    @CacheLookup
    public WebElement breadcrumbHome;

    @FindBy(css="a[href='/clientSetup']")
    @CacheLookup
    public WebElement breadcrumbClientSetup;

    @FindBy(css="a[href='/clientSetup/divisionmastersetup']")
    @CacheLookup
    public WebElement breadcrumbClientSetupDivisionMaster;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > div > div.row > div > button:nth-child(1)")
    WebElement addNew;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > div > div.row > div > button:nth-child(2)")
    WebElement Edit;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > div > div.row > div > button:nth-child(3)")
    WebElement Delete;

    @FindBy(css="#addClientDivision > div > h4")
    WebElement labelAddNewDivision;

    @FindBy(xpath = "//label[@for,('divisionName')]")
    WebElement labelName;

    @FindBy(css="#addClientDivision > div > div > div:nth-child(1) > button:nth-child(1)")
    WebElement btnSubmit;

    @FindBy(css="#addClientDivision > div > div > div:nth-child(1) > button:nth-child(2)")
    WebElement btnCancel;

    @FindBy(css="#addClientDivision > div > div > div:nth-child(1) > button:nth-child(3)")
    WebElement btnReset;

    @FindBy(id="divisionName")
    public WebElement inputDivisonName;

    @FindBy(xpath = "//a[text()='View Details']")
    public WebElement alert;



    public void verify_DivisionMasterSetupPageURL()
    {
        verifyURL(return_baseURL()+pageUrl);
        PageObjectLogging.log("verify_DivisionMasterSetupPageURL","Check the URL of DivisionMasterSetupPage",true,driver);


    }

    public void verify_division_master_page_title()
    {
         verifyPageHeading(strPageTitle, pageTitle);

    }

    public void verifyClientSetupLink()
    {
          VerifyBreadCrumbLinks(return_baseURL()+breadcrumbClientListLink, breadcrumbClientSetupDivisionMaster);
    }
    public void verifyDivisionMasterBCLink()
    {
          VerifyBreadCrumbLinks(return_baseURL()+breadcrumbClientSetupDivisionMasterLink,breadcrumbClientSetup);
    }

    public void verifyHomeBreadLink()
    {

        VerifyBreadCrumbLinks(return_baseURL()+breadCrumbHomeLink,breadcrumbHome);
    }

    public void verify_all_breadcrumb_links()
    {
        verifyClientSetupLink();
        verifyDivisionMasterBCLink();
        verifyHomeBreadLink();

    }

    public DivisionMasterSetupPageObjects clickAddNew() {
        waitForElementByElement(addNew);
        addNew.click();
        return this;
    }

    public DivisionMasterSetupPageObjects clickSubmit() {
        waitForElementByElement(btnSubmit);
        btnSubmit.click();
        return this;
    }

    public DivisionMasterSetupPageObjects setDivisionName(String DivisionName) {
        waitForElementByElement(inputDivisonName);
        inputDivisonName.sendKeys(DivisionName);
        return this;
    }

    public DivisionMasterSetupPageObjects verifyAlertMessage( ) {
       waitForElementByElement(alert);
       alert.click();
       waitForElementByXPath("//div[text()='Division Object created successfully']");
       return this;
    }
}
