package PageObjects.ClientSetUp;

import Common.Logging.PageObjectLogging;
import Common.core.Assertion;
import PageObjects.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by praveenk on 01-Mar-17.
 */
public class ClientListPageObjects extends BasePageObject {

    public ClientListPageObjects(WebDriver driver) {
        super(driver);
        columnNames.add(0,"Select");
        columnNames.add(1,"ClientName");
        columnNames.add(2,"ClientCode");
        columnNames.add(3,"State");
        columnNames.add(4,"City");


    }

    private final String pageUrl = "/clientSetup/clientList";

    private final String breadCrumbHomeLink="/home";

    private final String breadcrumbClientListLink = "/clientSetup/clientList";

    private final String breadcrumbClientSetupLink = "/clientSetup";

    private final String strPageTitle ="Clients Setup";

    public final String search_pageLoadedText = "1 - 1 of 1 Records";




    @FindBy(id="appTitle")
    public WebElement pageTitle;

    @FindBy(css = "a[href='/home']")
    @CacheLookup
    public WebElement breadcrumbHome;

    @FindBy(css="a[href='/clientSetup']")
    @CacheLookup
    public WebElement breadcrumbClientSetup;

    @FindBy(css="a[href='/clientSetup/clientList']")
    @CacheLookup
    public WebElement breadcrumbClientList;


    @FindBy(id = "logoutBt")
    @CacheLookup
    private WebElement logout;

    @FindBy(xpath = "//button[contains(., 'Add')]")
    public WebElement Add;

    @FindBy(xpath = "//button[contains(., 'Edit')]")
    public WebElement Edit;

    @FindBy(xpath = "//button[contains(., 'Delete')]")
    public WebElement Delete;

    @FindBy(xpath = "//button[contains(., 'View')]")
    public WebElement View;

    @FindBy(xpath = "//button[contains(., 'Prev')]")
    public WebElement Previous;

    @FindBy(xpath = "//button[contains(., 'Next')]")
    public WebElement Next;

    @FindBy(xpath = "//button[contains(., 'Reset')]")
    public WebElement Reset;

    @FindBy(css = "#list-container > table > tbody > tr > td:nth-child(1) > input")
    public WebElement  radio_select;



    @FindBy(xpath = "//input[@placeholder='Search client name']")
    public WebElement searchClient;

    @FindBy(xpath = "//select[@formcontrolname='selectedState']")
    public WebElement searchState;

    @FindBy(xpath = "//select[@formcontrolname='selectedCity']")
    public WebElement searchCity;

    @FindBy(css="#list-container > table > tbody")
    @CacheLookup
    public WebElement ClientTable;

    @FindBy(css="#list-container > table > tbody > tr")
    @CacheLookup
    public WebElement ClientTableRows;

    @FindBy(css="#list-container > table > tbody > tr > td")
    public WebElement ClientTableRowsCell;

    @FindBy(css="#list-container > table > thead  > tr > th")
    public List<WebElement> ClientTableHeader;

    @FindBy(css="#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > div > div:nth-child(3) > div.row > div:nth-child(2) > span")
    public WebElement pagination;

    ArrayList<String> columnNames = new ArrayList<String>();

    public ClientListPageObjects clickAdd( )
    {
        waitForElementByElement(Add);
        PageObjectLogging.log("clickAdd", "Add button clicked", true, driver);
        Add.click();
        return this;

    }

    public ClientListPageObjects clickReset( )
    {
        waitForElementByElement(Reset);
        PageObjectLogging.log("clickReset", "Reset button clicked", true, driver);
        Reset.click();
        return this;

    }

    public ClientListPageObjects verifyReset(String totalRecords)
    {
        waitForTextNotPresentInElementByElementLocatedBy(By.cssSelector("#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > div > div:nth-child(3) > div.row > div:nth-child(2) > span"),totalRecords);

        PageObjectLogging.log("verifyReset","Pagination text changed to "+pagination.getText() +"from "+totalRecords,true,driver);

        Reset.click();
        return this;

    }



    public ClientListPageObjects clickRadio()
    {
        waitForElementByElement(radio_select);
        PageObjectLogging.log("Input_user_id", "Inputting user id in the login box", true, driver);
        radio_select.click();
        return this;
    }

    public ClientListPageObjects clickEdit( )
    {
        waitForElementByElement(Edit);
        Edit.click();
        return this;

    }

    public void verify_ClientList_page_URL()
    {
      verifyURL(return_baseURL()+pageUrl);
      PageObjectLogging.log("verifyProgramSetupPageUrl","Check the URL of Client list page",true,driver);

    }



    public void  verify_ClientList_PageTitle()
    {

        verifyPageHeading(strPageTitle,pageTitle);

    }

    public void verifyHomeBreadLink()

    {
         VerifyBreadCrumbLinks(return_baseURL()+breadcrumbClientListLink,breadcrumbClientList);

     }
    public void verifyClientSetupLink()
    {


        VerifyBreadCrumbLinks(return_baseURL()+breadcrumbClientSetupLink,breadcrumbClientSetup);
    }

    public void verifyClientListLink()
    {
         VerifyBreadCrumbLinks(return_baseURL()+breadCrumbHomeLink,breadcrumbHome);
    }

    public void verify_all_breadcrumb_links()
    {
        verifyHomeBreadLink();
        verifyClientSetupLink();
        verifyClientListLink();


    }



    public boolean  verify_headers_present()
    {

        waitForElementByElement(pagination);
        ArrayList<String> RetrievedColumnNames=getHeaderName(ClientTableHeader);
        int match=0;
        for(int i=0;i<columnNames.size();i++){
            if(RetrievedColumnNames.get(i).equals(columnNames.get(i)))
            {
                PageObjectLogging.log("verify_headers_present","Found column header "+columnNames.get(i),true,driver);
                match++;

            }else{

                PageObjectLogging.log("verify_headers_present","Not Found column header "+columnNames.get(i),true,driver);

                    return false;
            }


        }

        return true;
    }

    public String get_total_Records()
    {
        waitForElementByElement(pagination);
        return pagination.getText();
    }

    public ClientListPageObjects search_Client(String ClientName)
    {
        waitForElementByElement(searchClient);
        searchClient.sendKeys(ClientName);
        PageObjectLogging.log("search_Client","Searching in the client list with the text "+ClientName,true);
        return this;
    }

    public List<HashMap<String, WebElement>> ClientTable_list_data()

    {

     // waitForElementByElement(ClientTable);

      List<HashMap<String, WebElement>> ClientListData= ReadTable_into_Hash();
      return ClientListData;
    }

    public void verify_search_result(String s, String totalRecords)
    {
       waitForTextNotPresentInElementByElementLocatedBy(By.cssSelector("#clientSetupBodyContainer > div > div:nth-child(2) > ng-component > div > div:nth-child(3) > div.row > div:nth-child(2) > span"),totalRecords);
       PageObjectLogging.log("verify_search_result","Pagination text changed to "+pagination.getText()+"from "+totalRecords,true,driver);
       // waitForTextToBePresentInElementByElement(pagination,"1 - 1 of 1 Records");
       List<HashMap<String, WebElement>> SearchResult= ClientTable_list_data();
       WebElement ResultElement = SearchResult.get(0).get("ClientName");
       Assertion.assertEquals(ResultElement.getText().toLowerCase( ),s);
       PageObjectLogging.log("verify_search_result","Result shown for searched text  "+ResultElement.getText(),true,driver);

    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the Client_setup class instance.
     */
    public ClientListPageObjects verifyPageLoaded() {
        (new WebDriverWait(driver, 15)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(search_pageLoadedText);
            }
        });
        return this;
    }

    public ClientListPageObjects verify_state_drop_down_shows_all_state()
    {

        int s=new Select(searchState).getOptions().size();
        Common.core.Assertion.assertNumber(40,s,"State Drop down check");
        PageObjectLogging.log("verify_state_drop_down_shows_all_state","Verify the State drop down count in Client List page is 40",true,driver);
        return this;

    }

    public void verify_buttons_list_page()
    {
        waitForElementByElement(Add);
        PageObjectLogging.log("verify_buttons_list_page","Add button is visible",true,driver);
        waitForElementNotClickableByElement(Edit);
        PageObjectLogging.log("verify_buttons_list_page","Edit button is visible",true,driver);
        waitForElementNotClickableByElement(Delete);
        PageObjectLogging.log("verify_buttons_list_page","Delete button is visible",true,driver);
        waitForElementNotClickableByElement(View);
        PageObjectLogging.log("verify_buttons_list_page","View button is visible",true,driver);
        waitForElementNotClickableByElement(Previous);
        PageObjectLogging.log("verify_buttons_list_page","Previous button is visible",true,driver);
        waitForElementByElement(Next);
        PageObjectLogging.log("verify_buttons_list_page","Next button is visible",true,driver);
    }




}
