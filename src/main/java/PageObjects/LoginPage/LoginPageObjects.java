package PageObjects.LoginPage;

import Common.Logging.PageObjectLogging;
import PageObjects.BasePageObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by praveenk on 09-Feb-17.
 */
public class LoginPageObjects extends BasePageObject {


    public LoginPageObjects(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#formBackround > form > div:nth-child(1) > input")
    public WebElement userName;

    @FindBy(xpath = "//*[@formcontrolname=\"password\"]")
    public WebElement passWord;

    @FindBy(id = "loginButton")
    public WebElement Loginbtn;

    @FindBy(xpath="//*[@id=\"formBackround\"]/form/div[2]")
    public WebElement alert_userName;

    @FindBy(css = "#formBackround > form > div:nth-child(3)")
    public WebElement alert_password;

    @FindBy(xpath = "//*[@id=\"formBackround\"]/form/div[4]")
    public WebElement errorAlert;







    public void input_username(String userid)
    {
        waitForElementByElement(userName);
        PageObjectLogging.log("Input_user_id", "Inputting user id in the login box", true, driver);
        sendKeys(userName,userid);

    }

    public void tab_press()
    {
        waitForElementByElement(userName);
        sendKeys_key(userName, Keys.TAB);
        PageObjectLogging.log("tab_pres", "Inputting user id in the login box", true, driver);
    }

    public void input_password(String password)
    {
        waitForElementByElement(passWord);
        PageObjectLogging.log("input_password", "Inputting admin_password in the login box", true, driver);
        sendKeys(passWord,password);

    }

    public void click_submit()
    {
        waitForElementClickableByElement(Loginbtn) ;
        Loginbtn.click();
    }

    public void login_phoenix(String userid,String pwd)
    {
        input_username(userid);
        input_password(pwd);
        click_submit();

    }

    public String capture_alert_username(String alert)
    {
        waitForTextToBePresentInElementByElement(alert_userName,alert);
        System.out.print(alert_userName.getText());
        return alert_userName.getText();

    }

    public String capture_alert_password(String alert)
    {
        waitForElementByElement(alert_password);
        waitForTextToBePresentInElementByElement(alert_password,alert);
        System.out.print(alert_password.getText());
        return alert_password.getText();

    }

    public String capture_error_alert(String alert)
    {
        waitForTextToBePresentInElementByElement(errorAlert,alert);
        System.out.print(errorAlert.getText());
        return errorAlert.getText();

    }


}
