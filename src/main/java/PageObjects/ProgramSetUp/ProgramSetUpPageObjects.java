package PageObjects.ProgramSetUp;

/**
 * Created by praveenk on 22-Mar-17.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Common.Logging.PageObjectLogging;
import PageObjects.BasePageObject;
import com.google.sitebricks.client.Web;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProgramSetUpPageObjects  extends BasePageObject {

    public ProgramSetUpPageObjects(WebDriver driver) {
        super(driver);
        columnNames.add(0,"Select");
        columnNames.add(1,"Program Name");
        columnNames.add(2,"Client name");
        columnNames.add(3,"Based On");
        columnNames.add(4,"Start Date");
        columnNames.add(5,"End Date");
    }
    public Map<String, String> data;
    ArrayList<String> columnNames = new ArrayList<String>();

    public int timeout = 15;

    @FindBy(id="appTitle")
    public WebElement pageTitle;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(1) a")
    @CacheLookup
    public WebElement about;

    @FindBy(css = "#programSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(3) div:nth-of-type(1) button:nth-of-type(1)")
    @CacheLookup
    public WebElement addNew;

    @FindBy(css = "#programSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(3) div:nth-of-type(1) button:nth-of-type(3)")
    @CacheLookup
    public WebElement deleteProgram;

    @FindBy(css = "#programSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(3) div:nth-of-type(1) button:nth-of-type(2)")
    @CacheLookup
    public WebElement editProgram;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(4) a")
    @CacheLookup
    public WebElement faq;

    @FindBy(css = "a[href='/home']")
    @CacheLookup
    public WebElement home;

    @FindBy(css="a[href='/programSetup/programSetupList']")
    public WebElement programSetupList;

    @FindBy(id = "logoutBt")
    @CacheLookup
    public WebElement logout;

    @FindBy(css = "#programSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(3) div:nth-of-type(1) button:nth-of-type(5)")
    @CacheLookup
    public WebElement next;

    public final String pageLoadedText = "Records";

    public final String pageUrl = "/programSetup/programSetupList";

    public final String strPageTitle ="Program Setup";

    @FindBy(css = "#programSetupBodyContainer div.innerPageContents div:nth-of-type(2) ng-component div:nth-of-type(3) div:nth-of-type(1) button:nth-of-type(4)")
    @CacheLookup
    public WebElement prev;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(2) a")
    @CacheLookup
    public WebElement privacy;

    @FindBy(css = "a.linkSelected")
    @CacheLookup
    public WebElement programList;

    @FindBy(css = "a[href='/programSetup']")
    @CacheLookup
    public WebElement programSetup;

    @FindBy(css = "#programSetupBodyContainer div.innerPageContents div:nth-of-type(1) div.col-lg-6.noPadding breadcrumbs div.breadcrumbs span:nth-of-type(3) a")
    @CacheLookup
    public WebElement programSetuplist;

    @FindBy(css = "#filter-container form.ng-untouched.ng-pristine.ng-valid div.form-group.row div:nth-of-type(3) button.innerPageButtons")
    @CacheLookup
    public WebElement reset;

    @FindBy(css="#programSetupBodyContainer > div > div:nth-child(2) > ng-component > div.row.tableCss > div > table > thead >tr > th")
    public List<WebElement> tableHeader;

    @FindBy(xpath = "//Select[formcontrolname=\"clientId\"]")
    public WebElement clientDropDown;

    @FindBy(xpath = "//Select[formcontrolname=\"basedOn\"]")
    public WebElement basedOnDropDown;


    @FindBy(css = "#filter-container form.ng-untouched.ng-pristine.ng-valid div.form-group.row div:nth-of-type(1) select.form-control.ng-untouched.ng-pristine.ng-valid")
    @CacheLookup
    public WebElement t000000120170314;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t000000120170315;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t000000120170318;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t00000020170317;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t00000020170320;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t00000020170321;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t00000020170322;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t00000020170325;

    @FindBy(css = "#filter-container form.ng-untouched.ng-pristine.ng-valid div.form-group.row div:nth-of-type(2) select.form-control.ng-untouched.ng-pristine.ng-valid")
    @CacheLookup
    public WebElement t000000220170314;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t000000220170315;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t000000220170318;

    @FindBy(name = "rId")
    @CacheLookup
    public List<WebElement> t000000320170314;


    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(3) a")
    @CacheLookup
    public WebElement termsOfUse;


    /**
     * Click on About Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickAboutLink() {
        about.click();
        return this;
    }

    /**
     * Click on Add New Button.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickAddNewButton() {
        addNew.click();
        return this;
    }

    /**
     * Click on Delete Program Button.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickDeleteProgramButton() {
        deleteProgram.click();
        return this;
    }

    /**
     * Click on Edit Program Button.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickEditProgramButton() {
        editProgram.click();
        return this;
    }

    /**
     * Click on Faq Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickFaqLink() {
        faq.click();
        return this;
    }

    /**
     * Click on Home Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickHomeLink() {
        home.click();
        return this;
    }

    /**
     * Click on Logout Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickLogoutLink() {
        logout.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickNextButton() {
        next.click();
        return this;
    }

    /**
     * Click on Prev Button.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickPrevButton() {
        prev.click();
        return this;
    }

    /**
     * Click on Privacy Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickPrivacyLink() {
        privacy.click();
        return this;
    }

    /**
     * Click on Program List Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickProgramListLink() {
        programList.click();
        return this;
    }

    /**
     * Click on Program Setup Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickProgramSetupLink() {
        programSetup.click();
        return this;
    }

    /**
     * Click on Program Setuplist Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickProgramSetuplistLink() {
        programSetuplist.click();
        return this;
    }

    /**
     * Click on Reset Button.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickResetButton() {
        reset.click();
        return this;
    }

    /**
     * Click on Terms Of Use Link.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects clickTermsOfUseLink() {
        termsOfUse.click();
        return this;
    }



    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */


    /**
     * Set default value to 20170314t000000 Drop Down List field.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects setT0000001DropDownListField20170314() {
        return setT0000001DropDownListField20170314(data.get("T000000_1_20170314"));
    }

    /**
     * Set value to 20170314t000000 Drop Down List field.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects setT0000001DropDownListField20170314(String t0000001Value20170314) {
        new Select(t000000120170314).selectByVisibleText(t0000001Value20170314);
        return this;
    }

   /**
     * Submit the form to target page.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */


    /**
     * Unset default value from 20170314t000000 Drop Down List field.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects unsetT0000001DropDownListField20170314() {
        return unsetT0000001DropDownListField20170314(data.get("T000000_1_20170314"));
    }

    /**
     * Unset value from 20170314t000000 Drop Down List field.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects unsetT0000001DropDownListField20170314(String t0000001Value20170314) {
        new Select(t000000120170314).deselectByVisibleText(t0000001Value20170314);
        return this;
    }




    /**
     * Unset value from 20170314t000000 Drop Down List field.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public void verifyBreadCrumbList()
    {
        waitForElementByElement(home);
        waitForElementByElement(programSetup);
        waitForElementByElement(programList);


    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */
    public ProgramSetUpPageObjects verifyProgramSetupPageUrl()
    {
        verifyURL(return_baseURL()+pageUrl);
        PageObjectLogging.log("verifyProgramSetupPageUrl","Check the URL of Program set up page",true,driver);
        return this;
    }

    /**
     * Verify that current page title  matches the expected title.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */

    public ProgramSetUpPageObjects  verifyProgramSetupPageTitle()
    {

        verifyPageHeading(strPageTitle,pageTitle);
        return this;

    }

    /**
     * Verify that table headers are correct.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */

    public boolean  verify_headers_present()
    {


        ArrayList<String> RetrievedColumnNames=getHeaderName(tableHeader);
        int match=0;
        for(int i=0;i<columnNames.size();i++){
            if(RetrievedColumnNames.get(i).equals(columnNames.get(i)))
            {
                PageObjectLogging.log("verify_headers_present","Found column header "+columnNames.get(i),true,driver);
                match++;

            }else{

                PageObjectLogging.log("verify_headers_present","Not Found column header "+columnNames.get(i),true,driver);

                return false;
            }


        }

        return true;
    }

    /**
     * Verify all buttons are shoing in the bottom of the table.
     *
     * @return the ProgramSetUpPageObjects class instance.
     */

    public ProgramSetUpPageObjects verify_buttons_list_page()
    {
        waitForElementByElement(addNew);
        PageObjectLogging.log("verify_buttons_list_page","Add button is visible",true,driver);
        waitForElementNotClickableByElement(editProgram);
        PageObjectLogging.log("verify_buttons_list_page","Edit button is visible",true,driver);
        waitForElementNotClickableByElement(deleteProgram);
        PageObjectLogging.log("verify_buttons_list_page","Delete button is visible",true,driver);
        waitForElementNotClickableByElement(prev);
        PageObjectLogging.log("verify_buttons_list_page","Previous button is visible",true,driver);
        waitForElementByElement(next);
        PageObjectLogging.log("verify_buttons_list_page","Next button is visible",true,driver);
        return this;
    }


    public List<HashMap<String, WebElement>> programListTable()

    {
       List<HashMap<String, WebElement>> programClients= ReadTable_into_Hash();
        return programClients;
    }
}

