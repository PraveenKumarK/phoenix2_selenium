package PageObjects.ProgramSetUp;

/**
 * Created by praveenk on 22-Mar-17.
 */
import java.util.HashMap;
import java.util.Map;

import PageObjects.BasePageObject;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProgramAddPageObjects extends BasePageObject{
      Map<String, String> data=new HashMap<String, String>()  ;

    private int timeout = 15;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(1) a")
    @CacheLookup
    private WebElement about;

    @FindBy(css = "#addBasicProgram div form.ng-untouched.ng-pristine.ng-invalid div:nth-of-type(10) button.innerPageButtons")
    @CacheLookup
    private WebElement addSlab;

    @FindBy(xpath = "//select[@formcontrolname=\"basedOn\"]")
    @CacheLookup
    private WebElement basedOn;

    @FindBy(css = "#addBasicProgram div div.row div:nth-of-type(1) button:nth-of-type(2)")
    @CacheLookup
    private WebElement cancel;

    @FindBy(xpath = "//select[@formcontrolname='claimEntryType']")
    @CacheLookup
    private WebElement claimEntryType;

    @FindBy(id = "programEndDate")
    @CacheLookup
    private WebElement endDate1;

    @FindBy(id = "programEndDate")
    @CacheLookup
    private WebElement endDate2;

    @FindBy(id = "claimPeriodEndDate")
    @CacheLookup
    private WebElement endDate3;

    @FindBy(id = "programEndDate")
    @CacheLookup
    private WebElement endDate4;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(4) a")
    @CacheLookup
    private WebElement faq;

    @FindBy(css = "a[href='/home']")
    @CacheLookup
    private WebElement home;

    @FindBy(xpath = "//select[@formcontrolname='isPrimarySalesAvailable']")
    @CacheLookup
    private WebElement isPrimarySalesAvailable;

    @FindBy(xpath = "//select[@formcontrolname='isThresholdSet']")
    @CacheLookup
    private WebElement isThresholdRequired;

    @FindBy(id = "logoutBt")
    @CacheLookup
    private WebElement logout;

    @FindBy(css = "#addBasicProgram div div.row div:nth-of-type(1) button:nth-of-type(4)")
    @CacheLookup
    private WebElement next;

    private final String pageLoadedText = "Terminology to use in program (Point / Coupon /Voucher)";

    private final String pageUrl = "/programSetup/basicProgramAdd";

    @FindBy(xpath = "//select[@formcontrolname='pointCalculationType']")
    @CacheLookup
    private WebElement pointCalculationType;

    @FindBy(id = "onePointIsEqualToRupees")
    @CacheLookup
    private WebElement pointRs1;

    @FindBy(xpath = "//select[@formcontrolname='pointsRoundUp']")
    @CacheLookup
    private WebElement pointsRoundup;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(2) a")
    @CacheLookup
    private WebElement privacy;

    @FindBy(css = "a[href='/programSetup/basicProgramAdd']")
    @CacheLookup
    private WebElement programBasicDetailsAdd;

    @FindBy(css = "a[href='/programSetup/programSetupList']")
    @CacheLookup
    private WebElement programList;

    @FindBy(id = "fieldName")
    @CacheLookup
    private WebElement programLogo1;

    @FindBy(id = "fieldName")
    @CacheLookup
    private WebElement programLogo2;

    @FindBy(id = "fieldName")
    @CacheLookup
    private WebElement programLogo3;

    @FindBy(id = "programName")
    @CacheLookup
    private WebElement programName1;

    @FindBy(xpath = "//input[@formcontrolname='programHelplineNo']")
    @CacheLookup
    private WebElement programHelplineNo;

    @FindBy(id = "slab")
    @CacheLookup
    private WebElement programPointsToRupeesrulesForBurning;

    @FindBy(css = "a[href='/programSetup']")
    @CacheLookup
    private WebElement programSetup;

    @FindBy(css = "#addBasicProgram div div.row div:nth-of-type(1) button:nth-of-type(3)")
    @CacheLookup
    private WebElement reset;

    @FindBy(xpath = "//select[@formcontrolname='clientId']")
    @CacheLookup
    private WebElement clientId;

    @FindBy(xpath = "//select[@formcontrolname='isPaymentQualificationSet']")
    @CacheLookup
    private WebElement setPaymentQualification;

    @FindBy(id = "programStartDate")
    @CacheLookup
    private WebElement startDate1;

    @FindBy(id = "transactionPeriodStartDate")
    @CacheLookup
    private WebElement startDate2;

    @FindBy(id = "claimPeriodStartDate")
    @CacheLookup
    private WebElement startDate3;

    @FindBy(id = "redemptionPeriodStartDate")
    @CacheLookup
    private WebElement startDate4;

    @FindBy(css = "#addBasicProgram div div.row div:nth-of-type(1) button:nth-of-type(1)")
    @CacheLookup
    private WebElement submit;

    @FindBy(xpath = "//select[@formcontrolname='isTaxInclusive']")
    @CacheLookup
    private WebElement tax;

    @FindBy(xpath = "//select[@formcontrolname='terminologyToUseInProgram']")
    @CacheLookup
    private WebElement terminologyToUseInProgram;

    @FindBy(css = "#appFooter div.row div:nth-of-type(1) div.footerLinks span:nth-of-type(3) a")
    @CacheLookup
    private WebElement termsOfUse;

    public ProgramAddPageObjects(WebDriver driver) {
        super(driver);
        data.put("SELECT_CLIENT","relaxo");
        data.put("PROGRAM_NAME_1","Random");
        data.put("PROGRAM_NAME_2","9886869680");
        data.put("SET_PAYMENT_QUALIFICATION","No");
        data.put("TERMINOLOGY_TO_USE_IN_PROGRAM_POINT","Point");
        data.put("CLAIM_ENTRY_TYPE","Invoice");
        data.put("IS_PRIMARY_SALES_AVAILABLE","No");
        data.put("IS_THRESHOLD_REQUIRED","No");
        data.put("POINT_CALCULATION_TYPE","Both");
        data.put("POINTS_ROUNDUP","No");
        data.put("BASED_ON","Product");
        data.put("TAX","Exclusive");
        data.put("POINT_RS__1","20");
        data.put("PROGRAM_POINTS_TO_RUPEESRULES_FOR_BURNING","50");


    }



    /**
     * Click on About Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickAboutLink() {
        about.click();
        return this;
    }

    /**
     * Click on Add Slab Button.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickAddSlabButton() {
        addSlab.click();
        return this;
    }

    /**
     * Click on Cancel Button.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickCancelButton() {
        cancel.click();
        return this;
    }

    /**
     * Click on Faq Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickFaqLink() {
        faq.click();
        return this;
    }

    /**
     * Click on Home Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickHomeLink() {
        home.click();
        return this;
    }

    /**
     * Click on Logout Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickLogoutLink() {
        logout.click();
        return this;
    }

    /**
     * Click on Next Button.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickNextButton() {
        next.click();
        return this;
    }

    /**
     * Click on Privacy Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickPrivacyLink() {
        privacy.click();
        return this;
    }

    /**
     * Click on Program Basic Details Add Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickProgramBasicDetailsAddLink() {
        programBasicDetailsAdd.click();
        return this;
    }

    /**
     * Click on Program List Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickProgramListLink() {
        programList.click();
        return this;
    }

    /**
     * Click on Program Setup Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickProgramSetupLink() {
        programSetup.click();
        return this;
    }

    /**
     * Click on Reset Button.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickResetButton() {
        reset.click();
        return this;
    }

    /**
     * Click on Submit Button.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickSubmitButton() {
        submit.click();
        return this;
    }

    /**
     * Click on Terms Of Use Link.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects clickTermsOfUseLink() {
        termsOfUse.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects fill() {
        setSelectClientDropDownListField();
        setProgramName1TextField();
        setProgramName2TextField();
        setBasedOnDropDownListField();
        setClaimEntryTypeDropDownListField();
        setPointCalculationTypeDropDownListField();
        setTaxDropDownListField();
        setIsThresholdRequiredDropDownListField();
        setPointsRoundupDropDownListField();
        setIsPrimarySalesAvailableDropDownListField();
        setTerminologyToUseInProgramPointDropDownListField();
        setProgramPointsToRupeesrulesForBurningTextField();
        setPointRsNumberField1();
        setSetPaymentQualificationDropDownListField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the programAddPageObjects class instance.
     */


    /**
     * Set default value to Based On Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setBasedOnDropDownListField() {
        return setBasedOnDropDownListField(data.get("BASED_ON"));
    }

    /**
     * Set value to Based On Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setBasedOnDropDownListField(String basedOnValue) {
        new Select(basedOn).selectByVisibleText(basedOnValue);
        return this;
    }

    /**
     * Set default value to Claim Entry Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setClaimEntryTypeDropDownListField() {
        return setClaimEntryTypeDropDownListField(data.get("CLAIM_ENTRY_TYPE"));
    }

    /**
     * Set value to Claim Entry Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setClaimEntryTypeDropDownListField(String claimEntryTypeValue) {
        new Select(claimEntryType).selectByVisibleText(claimEntryTypeValue);
        return this;
    }

    /**
     * Set End Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setEndDate1DateField() {
        return this;
    }

    /**
     * Set End Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setEndDate2DateField() {
        return this;
    }

    /**
     * Set End Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setEndDate3DateField() {
        return this;
    }

    /**
     * Set End Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setEndDate4DateField() {
        return this;
    }

    /**
     * Set default value to Is Primary Sales Available Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setIsPrimarySalesAvailableDropDownListField() {
        return setIsPrimarySalesAvailableDropDownListField(data.get("IS_PRIMARY_SALES_AVAILABLE"));
    }

    /**
     * Set value to Is Primary Sales Available Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setIsPrimarySalesAvailableDropDownListField(String isPrimarySalesAvailableValue) {
        new Select(isPrimarySalesAvailable).selectByVisibleText(isPrimarySalesAvailableValue);
        return this;
    }

    /**
     * Set default value to Is Threshold Required Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setIsThresholdRequiredDropDownListField() {
        return setIsThresholdRequiredDropDownListField(data.get("IS_THRESHOLD_REQUIRED"));
    }

    /**
     * Set value to Is Threshold Required Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setIsThresholdRequiredDropDownListField(String isThresholdRequiredValue) {
        new Select(isThresholdRequired).selectByVisibleText(isThresholdRequiredValue);
        return this;
    }

    /**
     * Set default value to Point Calculation Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setPointCalculationTypeDropDownListField() {
        return setPointCalculationTypeDropDownListField(data.get("POINT_CALCULATION_TYPE"));
    }

    /**
     * Set value to Point Calculation Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setPointCalculationTypeDropDownListField(String pointCalculationTypeValue) {
        new Select(pointCalculationType).selectByVisibleText(pointCalculationTypeValue);
        return this;
    }

    /**
     * Set default value to 1 Point Rs. Number field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setPointRsNumberField1() {
        return setPointRsNumberField1(data.get("POINT_RS__1"));
    }

    /**
     * Set value to 1 Point Rs. Number field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setPointRsNumberField1(String pointRsValue1) {
        pointRs1.sendKeys(pointRsValue1);
        return this;
    }

    /**
     * Set default value to Points Roundup Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setPointsRoundupDropDownListField() {
        return setPointsRoundupDropDownListField(data.get("POINTS_ROUNDUP"));
    }

    /**
     * Set value to Points Roundup Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setPointsRoundupDropDownListField(String pointsRoundupValue) {
        new Select(pointsRoundup).selectByVisibleText(pointsRoundupValue);
        return this;
    }

    /**
     * Set Program Logo File field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramLogo1FileField() {
        return this;
    }

    /**
     * Set Program Logo File field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramLogo2FileField() {
        return this;
    }

    /**
     * Set Program Logo File field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramLogo3FileField() {
        return this;
    }

    /**
     * Set default value to Program Name Text field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramName1TextField() {
        return setProgramName1TextField(data.get("PROGRAM_NAME_1"));
    }

    /**
     * Set value to Program Name Text field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramName1TextField(String programName1Value) {
        programName1.sendKeys(programName1Value);
        return this;
    }

    /**
     * Set default value to Program Name Text field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramName2TextField() {
        return setProgramName2TextField(data.get("PROGRAM_NAME_2"));
    }

    /**
     * Set value to Program Name Text field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramName2TextField(String programName2Value) {
        programHelplineNo.sendKeys(programName2Value);
        return this;
    }

    /**
     * Set default value to Program Points To Rupeesrules For Burning Point Text field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramPointsToRupeesrulesForBurningTextField() {
        return setProgramPointsToRupeesrulesForBurningTextField(data.get("PROGRAM_POINTS_TO_RUPEESRULES_FOR_BURNING"));
    }

    /**
     * Set value to Program Points To Rupeesrules For Burning Point Text field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setProgramPointsToRupeesrulesForBurningTextField(String programPointsToRupeesrulesForBurningValue) {
        programPointsToRupeesrulesForBurning.sendKeys(programPointsToRupeesrulesForBurningValue);
        return this;
    }

    /**
     * Set default value to Select Client Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setSelectClientDropDownListField() {
        return setSelectClientDropDownListField(data.get("SELECT_CLIENT"));
    }

    /**
     * Set value to Select Client Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setSelectClientDropDownListField(String selectClientValue) {
        new Select(clientId).selectByVisibleText(selectClientValue);
        return this;
    }

    /**
     * Set default value to Set Payment Qualification Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setSetPaymentQualificationDropDownListField() {
        return setSetPaymentQualificationDropDownListField(data.get("SET_PAYMENT_QUALIFICATION"));
    }

    /**
     * Set value to Set Payment Qualification Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setSetPaymentQualificationDropDownListField(String setPaymentQualificationValue) {
        new Select(setPaymentQualification).selectByVisibleText(setPaymentQualificationValue);
        return this;
    }

    /**
     * Set Start Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setStartDate1DateField() {
        return this;
    }

    /**
     * Set Start Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setStartDate2DateField() {
        return this;
    }

    /**
     * Set Start Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setStartDate3DateField() {
        return this;
    }

    /**
     * Set Start Date Date field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setStartDate4DateField() {
        return this;
    }

    /**
     * Set default value to Tax Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setTaxDropDownListField() {
        return setTaxDropDownListField(data.get("TAX"));
    }

    /**
     * Set value to Tax Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setTaxDropDownListField(String taxValue) {
        new Select(tax).selectByVisibleText(taxValue);
        return this;
    }

    /**
     * Set default value to Terminology To Use In Program Point Coupon Voucher Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setTerminologyToUseInProgramPointDropDownListField() {
        return setTerminologyToUseInProgramPointDropDownListField(data.get("TERMINOLOGY_TO_USE_IN_PROGRAM_POINT"));
    }

    /**
     * Set value to Terminology To Use In Program Point Coupon Voucher Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects setTerminologyToUseInProgramPointDropDownListField(String terminologyToUseInProgramPointValue) {
        new Select(terminologyToUseInProgram).selectByVisibleText(terminologyToUseInProgramPointValue);
        return this;
    }



    /**
     * Unset default value from Based On Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetBasedOnDropDownListField() {
        return unsetBasedOnDropDownListField(data.get("BASED_ON"));
    }

    /**
     * Unset value from Based On Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetBasedOnDropDownListField(String basedOnValue) {
        new Select(basedOn).deselectByVisibleText(basedOnValue);
        return this;
    }

    /**
     * Unset default value from Claim Entry Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetClaimEntryTypeDropDownListField() {
        return unsetClaimEntryTypeDropDownListField(data.get("CLAIM_ENTRY_TYPE"));
    }

    /**
     * Unset value from Claim Entry Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetClaimEntryTypeDropDownListField(String claimEntryTypeValue) {
        new Select(claimEntryType).deselectByVisibleText(claimEntryTypeValue);
        return this;
    }

    /**
     * Unset default value from Is Primary Sales Available Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetIsPrimarySalesAvailableDropDownListField() {
        return unsetIsPrimarySalesAvailableDropDownListField(data.get("IS_PRIMARY_SALES_AVAILABLE"));
    }

    /**
     * Unset value from Is Primary Sales Available Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetIsPrimarySalesAvailableDropDownListField(String isPrimarySalesAvailableValue) {
        new Select(isPrimarySalesAvailable).deselectByVisibleText(isPrimarySalesAvailableValue);
        return this;
    }

    /**
     * Unset default value from Is Threshold Required Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetIsThresholdRequiredDropDownListField() {
        return unsetIsThresholdRequiredDropDownListField(data.get("IS_THRESHOLD_REQUIRED"));
    }

    /**
     * Unset value from Is Threshold Required Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetIsThresholdRequiredDropDownListField(String isThresholdRequiredValue) {
        new Select(isThresholdRequired).deselectByVisibleText(isThresholdRequiredValue);
        return this;
    }

    /**
     * Unset default value from Point Calculation Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetPointCalculationTypeDropDownListField() {
        return unsetPointCalculationTypeDropDownListField(data.get("POINT_CALCULATION_TYPE"));
    }

    /**
     * Unset value from Point Calculation Type Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetPointCalculationTypeDropDownListField(String pointCalculationTypeValue) {
        new Select(pointCalculationType).deselectByVisibleText(pointCalculationTypeValue);
        return this;
    }

    /**
     * Unset default value from Points Roundup Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetPointsRoundupDropDownListField() {
        return unsetPointsRoundupDropDownListField(data.get("POINTS_ROUNDUP"));
    }

    /**
     * Unset value from Points Roundup Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetPointsRoundupDropDownListField(String pointsRoundupValue) {
        new Select(pointsRoundup).deselectByVisibleText(pointsRoundupValue);
        return this;
    }

    /**
     * Unset default value from Select Client Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetSelectClientDropDownListField() {
        return unsetSelectClientDropDownListField(data.get("SELECT_CLIENT"));
    }

    /**
     * Unset value from Select Client Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetSelectClientDropDownListField(String selectClientValue) {
        new Select(clientId).deselectByVisibleText(selectClientValue);
        return this;
    }

    /**
     * Unset default value from Set Payment Qualification Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetSetPaymentQualificationDropDownListField() {
        return unsetSetPaymentQualificationDropDownListField(data.get("SET_PAYMENT_QUALIFICATION"));
    }

    /**
     * Unset value from Set Payment Qualification Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetSetPaymentQualificationDropDownListField(String setPaymentQualificationValue) {
        new Select(setPaymentQualification).deselectByVisibleText(setPaymentQualificationValue);
        return this;
    }

    /**
     * Unset default value from Tax Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetTaxDropDownListField() {
        return unsetTaxDropDownListField(data.get("TAX"));
    }

    /**
     * Unset value from Tax Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetTaxDropDownListField(String taxValue) {
        new Select(tax).deselectByVisibleText(taxValue);
        return this;
    }

    /**
     * Unset default value from Terminology To Use In Program Point Coupon Voucher Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetTerminologyToUseInProgramPointDropDownListField() {
        return unsetTerminologyToUseInProgramPointDropDownListField(data.get("TERMINOLOGY_TO_USE_IN_PROGRAM_POINT"));
    }

    /**
     * Unset value from Terminology To Use In Program Point Coupon Voucher Drop Down List field.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects unsetTerminologyToUseInProgramPointDropDownListField(String terminologyToUseInProgramPointValue) {
        new Select(terminologyToUseInProgram).deselectByVisibleText(terminologyToUseInProgramPointValue);
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the programAddPageObjects class instance.
     */
    public ProgramAddPageObjects verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}

